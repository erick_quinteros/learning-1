##########################################################
#
#
# aktana- messageSequence estimates estimates Aktana Learning Engines.
#
# description: driver pgm
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(data.table)
library(text2vec)
library(fpc)
library(RMySQL)
library(futile.logger)
library(reticulate)
library(dplyr)
library(jiebaR)
library(foreach)
library(doMC)
library(parallel)

THRESHOLD_TO_CLUSTER <- 3  # row dimension of distance matrix for clustering
TOP_N <- 3  # top N keywords

numberCores <- detectCores() - 4
numberCores <- ifelse(numberCores <= 0, 1, numberCores) # set default 

################################################ 
# function: get message topic labels in Chinese
# Input: 
#        messageTopic
#        products
# Return:
#        results
################################################ 

get_message_topic_labels_cn <- function(messageTopic, products)
{
  registerDoMC(numberCores)
  
  setnames(products, "externalId", "productUID")
  
  #messageTopic <- merge(messageTopic, products, by="productUID")
  
  # group by productUID & emailTopicId and concatenate text of each message
  # then retrieve keywords of each cluster text
  messageTopicCluster <- messageTopic[, lapply(.(text), paste0, collapse=" "), by = c("productUID", "emailTopicId")]
  setnames(messageTopicCluster, "V1", "text")
  
  starter <- Sys.time()

  # multi-core parallel processing

  # initialize worker using topn keywords
  keys <- worker("keywords", topn = TOP_N)
      
  # function to get email topic name    
  getEmailTopicName <- function(text) {
      # retrieve keywords
      words <- keywords(text, keys)
      return(paste0(words, collapse = " "))
  }

  messageTopicCluster[, emailTopicName := mcmapply(getEmailTopicName, text, mc.cores=numberCores)]
  messageTopicCluster$text <- NULL
  
  # merge cluster containing keywords into messageTopic
  messageTopic <- merge(messageTopic, messageTopicCluster, by = c("productUID", "emailTopicId"))
  messageTopic <- messageTopic[, .(messageUID, documentDescription, emailSubject, productUID, emailTopicId, emailTopicName)]

  flog.info("parallel processing time is %s", Sys.time()-starter)

  return(messageTopic)
}

########################################
# function: messageClustering
########################################
messageClustering <- function (con_cs, con_l, con) {
  
  flog.info("start running messageClustering")

  # source python script
  use_python("/usr/bin/python3")
  source_python(sprintf("%s/messageSequence/message_cluster_labeler.py",homedir))
  
  dbGetQuery(con_cs,"SET NAMES utf8")
  
  # load data
  recordType <-  data.table(dbGetQuery(con_cs,"SELECT * FROM RecordType;"))
  approvedDoc <- data.table(dbGetQuery(con_cs,sprintf("SELECT * FROM Approved_Document_vod__c WHERE IsDeleted=0 and RecordTypeId='%s';",recordType[Name=="Email_Template_vod" & SobjectType=="Approved_Document_vod__c"]$Id)))
  
  cols <- names(approvedDoc)
  cols <- gsub("_vod__c","",cols)
  cols <- gsub("__c","",cols)
  names(approvedDoc) <- cols
  
  approveD <- approvedDoc[,c("Id","Name","Document_Description","Email_Subject","Status","Product","Email_HTML_1"),with=F]
  
  prods <- unique(approveD$Product)
  AP <- approveD
  
  # do the analysis on the concatenation of the document description and the email subject
  AP[,Name:=paste(Document_Description,Email_Subject,sep=" ")]
  AP$Email_HTML_1 <- NULL
  
  # cleanup text to be used for the analysis
  AP$Name <- tolower(AP$Name)
  AP$Name <- gsub(":","",AP$Name)
  AP$Name <- gsub("-","",AP$Name)
  AP$Name <- gsub("_","",AP$Name)
  AP$Name <- gsub("&"," and ",AP$Name)
  AP$Name <- gsub("^ *|(?<= ) | *$", "", AP$Name, perl = TRUE)
  AP$Name <- gsub("\n","",AP$Name)
  AP$Name <- gsub(".com","",AP$Name)
  AP$Name <- gsub("nbsp;","",AP$Name)

  # use customer name convention to determin if the customer is of Chinese language
  isChinese <- endsWith(toupper(customer), "CN")

  if (isChinese) {
      user.dict <- sprintf("%s/messageSequence/user_dict.txt", homedir)
      jieba<-jiebaR::worker(user=user.dict)

      tok_fun <-function(strings) 
          plyr::llply(strings, segment, jieba)

      data <- fread(sprintf("%s/messageSequence/stop_words_file.csv", homedir), header = F)
      stop_words <- data$V1
  }

  # analyze each product message collection separately
  startTimer <- Sys.time()
  messageTopic <- data.table()
  for(i in prods)                                                              # separate analysis for each product
  {
    ad <- AP[Product==i]

    if (isChinese) {
        train <- itoken(ad$Name,preprocessor=identity, tokenizer=tok_fun, ids=ad$Id, progressbar=F) # tokenize Chinese language
        vocab <- create_vocabulary(train,ngram=c(1L,2L), stopwords = stop_words)                    # create a vocab of one-word and 2-word ngrams
    } else {
        train <- itoken(ad$Name,preprocessor=tolower,tokenizer=word_tokenizer,ids=ad$Id,progressbar=F)  # pull out the tokens from the text to be analyzed
        vocab <- create_vocabulary(train,ngram=c(1L,2L))                                                # create a vocab of one-word and 2-word ngrams
    }

    vectorizer <- vocab_vectorizer(vocab)                                                           # vectorize the vocabulary
    vocab <- prune_vocabulary(vocab,term_count_max=50L,doc_proportion_max=.50)                      # prune the vocabulary removing words that appear often and in many messages

    if(nrow(vocab) == 0) next                                                                       # skip empty vocab

    dtm <- create_dtm(train,vectorizer)                                                             # create a document term matrix
    mma <- dist2(dtm,method="cosine")                                                               # find the distance between each document based on the dtm
    
    if(dim(mma)[1] <= THRESHOLD_TO_CLUSTER) next                                                    # skip if there are few words

    print("start clustering ...")
    hc <- pamk(mma,krange=1:(dim(mma)[1]-1))$pamobject$clustering                                   # use partitioning around medoids to create clustering
    print("end clustering.")
    t <- data.table(Id=names(hc),cluster=hc)
    
    ap <- ad[,c("Id","Document_Description","Email_Subject"),with=F]
    ap <- merge(ap,t,by="Id")                                                                       # save the results
    names(ap) <- c("messageUID","documentDescription","emailSubject","emailTopicId")
    ap$productUID <- i
    messageTopic <- rbind(ap,messageTopic,fill=T)                                                   # messageTopic table contains results for all products
  }
  # done with analysis
  flog.info("get messageTopic Time = %s", Sys.time()-startTimer)

  # fill in for non-scored messages
  AP[,c("Name","Status"):= NULL]
  
  if (isChinese) {
    # processing non-clustered Chinese messages
    names(AP) <- c("messageUID","documentDescription","emailSubject","productUID")
    messagesAssigned <- messageTopic$messageUID
    
    # get the messages not being clustered
    t <- AP[!messageUID %in% messagesAssigned]  
    
    # create a sequence starting from maxId+1
    s <- seq(from = max(messageTopic$emailTopicId)+1, length.out = nrow(t)) 
    
    # assign the sequence number as new cluster id
    t$emailTopicId <- s
    
    # combine
    messageTopic <- rbind(messageTopic,t)
    
  } else {  
    # throw all non-clustered messages into one group
    AP$emailTopicId <- max(messageTopic$emailTopicId)+1                                               
    names(AP) <- c("messageUID","documentDescription","emailSubject","productUID","emailTopicId")
    messagesAssigned <- messageTopic$messageUID
    t <- AP[!messageUID %in% messagesAssigned]
    messageTopic <- rbind(messageTopic,t)
  }
  
  dbGetQuery(con,"SET NAMES utf8;")
  products <- dbGetQuery(con, 'SELECT productName, externalId FROM Product;')
  
  if (isChinese) {
     # get message topic labels in Chinese
    messageTopic[, text := paste0(documentDescription, emailSubject, sep=" ")]
    
    # generate Chinese label on email topics
    messageTopic <- get_message_topic_labels_cn(messageTopic, products)
    
  } else {
    # call python function to get the "emailTopicName"
    # messageTopic$emailTopicName <- paste(messageTopic$emailTopicId, messageTopic$productUID, sep="_")
    messageTopicNames <- generate_message_cluster_labels(messageTopic, products)
    messageTopic <- merge(messageTopic, messageTopicNames, by=c("productUID","emailTopicId"), sort=FALSE, all.x=TRUE)
  }

  # save results
  FIELDS <- list(messageUID="varchar(40)",productUID="varchar(40)",documentDescription="varchar(255)",emailSubject="varchar(255)",emailTopicId="int(11)", emailTopicName="varchar(255)")
  
  dbWriteTable(con_l,name="AKT_Message_Topic_Email_Learned", value = as.data.frame(messageTopic), overwrite=T, append=F, row.names=FALSE,field.types=FIELDS)
  
  flog.info('Return from messageClustering')
}
