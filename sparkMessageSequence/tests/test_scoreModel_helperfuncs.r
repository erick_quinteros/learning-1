context('testing scoreModel.processInts, scoreModel.prepareDesignMatrix, scoreModel.scoreMessage, scoreModel.getScoresLowerUpperBound defined in scoreModel script in spark MSO module')
print(Sys.time())

# load library, loading common source scripts
library(h2o)
library(uuid)
library(Learning)
library(openxlsx)
library(sparkLearning)
library(sparklyr)
library(arrow)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFuncSpark.r',homedir))
source(sprintf("%s/sparkMessageSequence/scoreModel.R",homedir))

# connect to spark
sc <- initializeSpark(homedir)

####### test for scoreModel.processInts
test_that("test scoreModel.processInts", {
  # load required input
  load(sprintf('%s/messageSequence/tests/data/from_whiteList_whiteListDB.RData', homedir)) # whiteListDB
  whiteList <- whiteListDB$accountId
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactions.RData', homedir))
  # convert to spark data frame
  interactions$date <- as.character(interactions$date)
  interactions <- sdf_copy_to(sc, interactions, "interactions", overwrite=TRUE, memory=TRUE)
  interactions <- interactions %>% mutate(date = to_date(date))
  # run function
  ints_new <- scoreModel.processInts(whiteList, interactions)
  # run tests
  expect_equal(sdf_dim(ints_new), c(2098,9))
  # test same as saved
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_processInts_ints.RData', homedir))
  ints_new <- convertSDFToDF(ints_new)
  setkey(ints, repId,accountId,date,key,interactionId)
  setkey(ints_new, repId,accountId,date,key,interactionId)
  expect_equal(ints_new, ints)
})

####### test for scoreModel.prepareDesignMatrix
test_that("test scoreModel.prepareDesignMatrix", {
  # load required input
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_AP.RData',homedir))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_processInts_ints.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  scoreDate <- as.Date(readModuleConfig(homedir, 'messageSequence','scoreDate')) # fix scoreDate so the result is always the same
  DAYS_CONSIDER_FOR_PRIORVISIT <- 30
  # convert to spark data frame
  AP <- sdf_copy_to(sc, AP, "AP", overwrite=TRUE, memory=TRUE)
  ints$date <- as.character(ints$date)
  ints <- sdf_copy_to(sc, ints, "interactions", overwrite=TRUE, memory=TRUE)
  ints <- ints %>% mutate(date = to_date(date))
  interactionsP$date <- as.character(interactionsP$date)
  interactionsP <- sdf_copy_to(sc, interactionsP, "interactionsP", overwrite=TRUE, memory=TRUE)
  interactionsP <- interactionsP %>% mutate(date = to_date(date))
  # run function
  allModel_new <- scoreModel.prepareDesignMatrix(AP, ints, interactionsP, scoreDate, DAYS_CONSIDER_FOR_PRIORVISIT)
  # run tests
  expect_equal(sdf_dim(allModel_new), c(309,754))
  # check saved as samed
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_prepareDesignMatrix_allModel.RData', homedir))
  allModel_new <- convertSDFToDF(allModel_new)
  setkey(allModel_new, accountId)
  expect_equal(allModel_new, allModel)
})

##### test for scoreModel.getAcctsToReScore
test_that("test scoreModel.getAcctsToReScore", {
  # required data from DB to get input
  requiredMockDataList <- list(pfizerusdev=c('MessageSet','MessageAlgorithm','MessageSetMessage'),pfizerusdev_learning=c('MessageRescoringTimes'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
  # load required input
  tgtName <- 'a3RA00000001MtAMAU'
  msgId <- 703
  rescoringLimit <- readModuleConfig(homedir, 'messageSequence','rescoringLimit')
  percentageMessages <- readModuleConfig(homedir, 'messageSequence','percentageMessages')
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSetMessage.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  intsSEND <- unique(interactionsP[productInteractionTypeName=="SEND" & !is.na(physicalMessageUID)][,c("accountId","physicalMessageUID","date")])
  ints_acct_msg_ldate <- intsSEND[, .(latestDate=max(date)), by=.(accountId,physicalMessageUID)]
  BUILD_UID <- readModuleConfig(homedir, 'messageSequence','buildUID')
  modelsToScore <- data.table(dbGetQuery(con,"SELECT ms.messageSetId, ms.messageAlgorithmId, ma.externalId FROM MessageSet ms JOIN (SELECT messageAlgorithmId, externalId FROM MessageAlgorithm where isDeleted=0 and isActive=1 and messageAlgorithmType=1) ma ON ms.messageAlgorithmId=ma.messageAlgorithmId;"))
  msgAlgoId <- unique(modelsToScore$messageAlgorithmId)[1]
  msIds <- modelsToScore[messageAlgorithmId==msgAlgoId]$messageSetId 
  msgIds <- messageSetMessage[messageSetId %in% msIds]$messageId
  msm <- messageSetMessage[messageSetId %in% msIds]  # messageSetMessage filtering with msgAlgoId and those messageSetIds
  msmCnt <- msm[, .N, by="messageSetId"]             # group by messageSetId; count of messages in each messageSetId
  msm <- merge(msm, msmCnt)                          # tuple(messageSetId, messageId, Count) 
  msmMinCnt <- msm[, .(cnt=min(N)), by="messageId"] # tuple(messageId, minimum count)
  sparkDBconURL_l <- sparkGetDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  messageRescoringTimesSQL <- sprintf("SELECT accountId, physicalMessageUID, messageId, learningBuildUID, rescoringTimes, createdAt, updatedAt FROM MessageRescoringTimes WHERE learningBuildUID='%s'",BUILD_UID)
  messageRescoringTimes <- sparkReadFromDB(sc, sparkDBconURL_l, messageRescoringTimesSQL, "messageRescoringTimes", memory=TRUE)
  rm(interactionsP, modelsToScore, msgAlgoId, msIds, msgIds, msm, BUILD_UID)
  dbDisconnect(con)
  # send data to spark
  intsSEND$date <- as.character(intsSEND$date)
  intsSEND <- sdf_copy_to(sc, intsSEND, "intsSEND", overwrite=TRUE, memory=TRUE)
  intsSEND <- intsSEND %>% mutate(date=to_date(date))
  ints_acct_msg_ldate$date <- as.character(ints_acct_msg_ldate$date)
  ints_acct_msg_ldate <- sdf_copy_to(sc, ints_acct_msg_ldate, "ints_acct_msg_ldate", overwrite=TRUE, memory=TRUE)
  ints_acct_msg_ldate <- ints_acct_msg_ldate %>% mutate(date=to_date(date))
  # run function
  accts_rescore_new <- scoreModel.getAcctsToReScore(ints_acct_msg_ldate, intsSEND, messageRescoringTimes, msmMinCnt, rescoringLimit, percentageMessages, tgtName, msgId)
  # run tests
  expect_length(accts_rescore_new, 96)
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_getAcctsToReScore_accts_rescore.RData', homedir))
  expect_equal(sort(accts_rescore_new), sort(accts_rescore))
})

# close spark connection
closeSpark(sc)

##### test for scoreModel.scoreMessage
test_that("test scoreModel.scoreMessage", {
  # load required input
  BUILD_UID <- readModuleConfig(homedir, 'messageSequence','buildUID')
  setupMockBuildDir(homedir, 'messageSequence', BUILD_UID)
  targetName <- "OPEN___a3RA00000001MtAMAU"
  msgId <- 703
  modelsaveSpreadsheetName <- sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID)
  models <- data.table(read.xlsx(modelsaveSpreadsheetName,sheet=3))                         # models table contains the references to the models
  load(sprintf('%s/messageSequence/tests/data/for_scoreModel_scoreMessage_acctsToScore.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_prepareDesignMatrix_allModel.RData', homedir))
  t <- allModel
  sendName <- 'SEND___a3RA00000001MtAMAU'
  if(!sendName %in% names(t)) { t[,(sendName):=0] }
  if(!targetName %in% names(t)) {t[,(targetName):=0]}
  
  # start h2o which required for function
  suppressWarnings(h2o.init(nthreads=-1, max_mem_size="24g"))
  # run function
  tp_new <- scoreModel.scoreMessage(t, acctsToScore, targetName, models, msgId)
  # run tests
  expect_equal(dim(tp_new), c(88,7))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_scoreMessage_tp.RData', homedir))
  expect_equal(tp_new, tp)
})

##### test fro scoreModel.getScoresLowerUpperBound
test_that("test scoreModel.getScoresLowerUpperBound", {
  # load required input
  load(sprintf('%s/messageSequence/tests/data/for_getScoresLowerUpperBound_scores.RData', homedir))
  # run function & tests
  tmp <- scoreModel.getScoresLowerUpperBound(scores, "Conservative")
  expect_equal(tmp, list(0.0021113096423771, 0.085927580395794))
  tmp <- scoreModel.getScoresLowerUpperBound(scores, "Moderate")
  expect_equal(tmp, list(0.044019445019086, 0.20017161726028))
  tmp <- scoreModel.getScoresLowerUpperBound(scores, "Aggressive")
  expect_equal(tmp, list(0.085927580395794, 0.31441565412477))
  tmp <- scoreModel.getScoresLowerUpperBound(scores, "Other_invalid_string")
  expect_equal(tmp, list(0.0021113096423771, 0.085927580395794))
})