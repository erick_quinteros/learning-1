context('test the scoreModel func in the spark MSO module')
print(Sys.time())

# load library, loading common source scripts
library(h2o)
library(uuid)
library(Learning)
library(sparkLearning)
library(sparklyr)
library(arrow)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFuncSpark.r',homedir))
source(sprintf("%s/sparkMessageSequence/scoreModel.R",homedir))

# start h2o
print("start h2o early")
tryCatch(suppressWarnings(h2o.init(nthreads=-1, max_mem_size="24g")), 
         error = function(e) {
           flog.error('Error in h2o.init()',name='error')
           quit(save = "no", status = 65, runLast = FALSE) # user-defined error code 65 for failure of h2o initialization
         })

############################### func for reset mock data for run scoreModel #######################################
runScoreModelMockDataReset <- function() {
  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('RepAccountAssignment','Account','Product','AccountProduct','MessageSet','MessageAlgorithm','MessageSetMessage'),pfizerusdev_learning=c('LearningObjectList','AccountMessageSequence','MessageRescoringTimes'))
  # 'MessageSet','MessageAlgorithm','MessageSetMessage' is for adding necessary param to run scoreModel
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
  
  # set up unit test build folder and related configs
  # get buildUID
  buildUID <- readModuleConfig(homedir, 'messageSequence','buildUID')
  # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  setupMockBuildDir(homedir, 'messageSequence',buildUID)
  return (buildUID)
}

########################### func for prepare arguments necessary for run scoreModel func and run ###################
# establish a func to run the setup and call scoreModel as it will be used multiple times for different cases
runScoreModel <- function(isNightly, flagRescoring, buildUID) {
  # get arugments requried for logging & aruguments required for running the script (common for nightly and manual scoring)
  BUILD_UID <- buildUID
  DRIVERMODULE <- "messageScoreDriver.r"
  RUN_UID <- UUIDgenerate()
  config <- initializeConfigurationNew(homedir, BUILD_UID)
  runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=NULL, createExcel=TRUE, createPlotDir=TRUE)
  runSettings[["modelsaveSpreadsheetName"]] <- sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID)
  scoreDate <- as.Date(readModuleConfig(homedir, 'messageSequence','scoreDate')) # fix scoreDate so the result is always the same
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  sparkDBconURL_l <- sparkGetDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  load(sprintf('%s/messageSequence/tests/data/from_whiteList_whiteListDB.RData', homedir)) # whiteListDB
  whiteList <- whiteListDB$accountId
  # load result data from loadMessageSequenceData
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactions.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountProduct.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_products.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNames.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messages.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSetMessage.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_expiredMessages.RData', homedir))
  
  # arguments needed to run scoreModel (used in nightly scoring flagRescoring=TRUE)
  msmMinCnt <- NULL
  
  # get arguments for manually and nightly run separately & run scirpt
  if (!isNightly) {
    TargetNames <- c('a3RA00000001MtAMAU', 'a3RA0000000e0u5MAA', 'a3RA0000000e0wBMAQ', 'a3RA0000000e486MAA', 'a3RA0000000e47gMAA')
  } else { # param needed for nightly run
    con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
    # get target names
    productUID <- getConfigurationValueNew(config, "productUID")
    pName <- products[externalId==productUID]$productName
    modelsToScore <- data.table(dbGetQuery(con,"SELECT ms.messageSetId, ms.messageAlgorithmId, ma.externalId FROM MessageSet ms JOIN (SELECT messageAlgorithmId, externalId FROM MessageAlgorithm where isDeleted=0 and isActive=1 and messageAlgorithmType=1) ma ON ms.messageAlgorithmId=ma.messageAlgorithmId;"))
    msgAlgoId <- unique(modelsToScore$messageAlgorithmId)[1]
    msIds <- modelsToScore[messageAlgorithmId==msgAlgoId]$messageSetId 
    msgIds <- messageSetMessage[messageSetId %in% msIds]$messageId
    TargetNames <- unique(messages[productName==pName & !is.na(lastPhysicalMessageUID) & messageId %in% msgIds]$lastPhysicalMessageUID)
    # get if flagRescoring=TRUE, rescoring parameters
    if (flagRescoring) {
      # build a table for each message with minimum count of number of messages in corresponding messageSets to be scored.
      # e.g. if msg# 20 belongs to messageSet 1 (containing 6 msgs) and messageSet2 (containing 8 msgs), the tuple should be (20, 6) 
      msm <- messageSetMessage[messageSetId %in% msIds]  # messageSetMessage filtering with msgAlgoId and those messageSetIds
      msmCnt <- msm[, .N, by="messageSetId"]             # group by messageSetId; count of messages in each messageSetId
      msm <- merge(msm, msmCnt)                          # tuple(messageSetId, messageId, Count) 
      msmMinCnt <- msm[, .(cnt=min(N)), by="messageId"] # tuple(messageId, minimum count)
    }
    dbDisconnect(con)
  }
  
  # send data to spark
  sc <- initializeSpark(homedir)
  interactions$date <- as.character(interactions$date)
  interactions <- sdf_copy_to(sc, interactions, "interactions", overwrite=TRUE, memory=TRUE)
  interactions <- interactions %>% mutate(date = to_date(date))
  interactionsP$date <- as.character(interactionsP$date)
  interactionsP <- sdf_copy_to(sc, interactionsP, "interactionsP", overwrite=TRUE, memory=TRUE)
  interactionsP <- interactionsP %>% mutate(date = to_date(date))
  accountProduct <- sdf_copy_to(sc, accountProduct, "accountProduct", overwrite=TRUE, memory=TRUE)
  
  # run script
  runDir <- runSettings[["runDir"]]
  modelsaveDir <- runSettings[["modelsaveDir"]]
  modelsaveSpreadsheetName <- runSettings[["modelsaveSpreadsheetName"]]
  result <- scoreModel(sparkDBconURL_l, con_l, isNightly, flagRescoring, runDir, modelsaveDir, modelsaveSpreadsheetName, BUILD_UID, config, scoreDate, TargetNames, whiteList, products, accountProduct, interactions, interactionsP, messages, expiredMessages, emailTopicNames, msmMinCnt)
  # db disconnect
  dbDisconnect(con_l)
  # final clean up
  closeClient()
  return(list(RUN_UID,result,sc))
}

################################### main test procedures and cases ######################################################
test_that("tests for manual scoring", {
  # arguments for manual scoring
  isNightly <- FALSE
  flagRescoring <- FALSE
  buildUID <- runScoreModelMockDataReset()
  temp <- runScoreModel(isNightly, flagRescoring, buildUID)
  result_new <- temp[[2]]
  runUID <- temp[[1]]
  sc <- temp[[3]]
  
  # test cases
  config <- initializeConfigurationNew(homedir, buildUID)
  # check calculation result is the same (as always use the same saved model and data)
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_manual_scores.RData', homedir))
  # load(sprintf('%s/messageSequence/tests/data/from_scoreModel_manual_messageRescoringTimesNew.RData', homedir))
  expect_equal(result_new[[1]],0)
  expect_equal(result_new[[2]],scores)
  expect_equal(result_new[[3]],NULL)
  
  # close spark
  closeSpark(sc)
  
  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/Design.RData',homedir,buildUID))
  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # check general log information
  ind <- grep('Score buildUID:',log_contents)
  expect_str_end_match(log_contents[ind],buildUID)
  ind <- grep('Analyzing product:',log_contents)
  expect_str_end_match(log_contents[ind],'CHANTIX')
  # check isNightly and flagRescoring flag
  ind <- sum(grepl('flagRescoring=FALSE isNightly=FALSE', log_contents))
  expect_equal(ind>=1, TRUE)
  # check message a3RA00000001MtAMAU
  ind <- grep('OPEN___a3RA00000001MtAMAU',log_contents)
  expect_length(ind,2) # have start analysis and analysis finish successfully
  ind <- ind[1]
  expect_str_end_match(log_contents[ind+1],'703') # msgId
  expect_str_end_match(log_contents[ind+2],'309') # before rm sent+target records to score
  expect_str_end_match(log_contents[ind+4],'88') # num of reocrds to score after rm
  # check message a3RA0000000e0u5MAA
  ind <- grep('OPEN___a3RA0000000e486MAA',log_contents)
  expect_length(ind,1) # as this is expired message, so skip scoring half way
  expect_str_pattern_find(log_contents[ind+1],'expired') # detect msg is expired
  # check random sampling
  ind <- grep('Messaging strategy:',log_contents)
  expect_str_end_match(log_contents[ind],config[["LE_MS_newMessageStrategy"]])
  # check message a3RA0000000e0wBMAQ
  ind <- grep('a3RA0000000e0wBMAQ',log_contents)
  expect_str_end_match(log_contents[ind[1]],'lower bound 0.000845584853835724 upper bound 0.0856545222273745') # check calculation
  expect_str_pattern_find(log_contents[ind[1]+1],'expired') # check detected as expired
  # check message a3RA0000000e0u5MAA
  ind <- grep('a3RA0000000e0u5MAA',log_contents)
  expect_str_end_match(log_contents[ind[1]],'lower bound 0.000845584853835724 upper bound 0.0856545222273745') # check calculation
  expect_str_pattern_find(log_contents[ind[1]+1],'expired') # check detected as expired
  # check message a3RA0000000e47gMAA
  ind <- grep('a3RA0000000e47gMAA',log_contents)
  expect_str_end_match(log_contents[ind[1]],'lower bound 0.000845584853835724 upper bound 0.0856545222273745') # check calculation
  expect_str_pattern_find(log_contents[ind[1]+1],'expired') # check detected as expired
})

test_that("tests for run second time manual scoring with no change data (should not score)", {
  # arguments for needed to run scoreModel again but not scoring (exists Design.RData, and AccountMessageSequence is not empty)
  drv <- dbDriver("MySQL")
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  SQL <- "Insert into AccountMessageSequence values ('test','test','test','test',0.9,'2012-12-01','2012-12-01');"
  dbClearResult(dbSendQuery(con_l, SQL))
  dbDisconnect(con_l)
  # run manual scoreModel again
  isNightly <- FALSE
  flagRescoring <- FALSE
  buildUID <- readModuleConfig(homedir, 'messageSequence','buildUID')
  temp <- runScoreModel(isNightly, flagRescoring, buildUID)
  runUID <- temp[[1]]
  result_new <- temp[[2]]
  sc <- temp[[3]]

  # test for results
  expect_equal(result_new, list(1,NULL,NULL))
  
  # close spark
  closeSpark(sc)
  
  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # check that scoring is skipped
  expect_str_end_match(log_contents[length(log_contents)],'Nothing to score so returning')
})


test_that("tests for nightly scoring with flagRescoring=TRUE", {
  # arguments for manual scoring
  isNightly <- TRUE
  flagRescoring <- TRUE
  buildUID <- runScoreModelMockDataReset()
  temp <- runScoreModel(isNightly, flagRescoring, buildUID)
  runUID <- temp[[1]]
  result_new <- temp[[2]]
  sc <- temp[[3]]

  # test cases
  config <- initializeConfigurationNew(homedir, buildUID)
  # check calculation result is the same (as always use the same saved model and data)
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_nightly_scores.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_nightly_messageRescoringTimesNew.RData', homedir))
  expect_equal(result_new[[1]],0)
  expect_equal(result_new[[2]],scores)
  expect_equal(convertSDFToDF(result_new[[3]])[order(accountId),-c("createdAt","updatedAt")],messageRescoringTimesNew[order(accountId),-c("createdAt","updatedAt")])
  
  # close spark
  closeSpark(sc)

  # check build dir structure
  expect_file_not_exists(sprintf('%s/builds/%s/Design.RData',homedir,buildUID))

  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # check general log information
  ind <- grep('Score buildUID:',log_contents)
  expect_str_end_match(log_contents[ind],buildUID)
  ind <- grep('Analyzing product:',log_contents)
  expect_str_end_match(log_contents[ind],'CHANTIX')
  # check rescoring parameter
  ind <- sum(grepl('RescorinTimes Limit is 2', log_contents))
  expect_equal(ind>=1, TRUE)
  # check message a3RA00000001MtAMAU
  ind <- grep('OPEN___a3RA00000001MtAMAU',log_contents)
  expect_length(ind,2) # have start analysis and analysis finish successfully
  ind <- ind[1]
  expect_str_end_match(log_contents[ind+1],'703') # msgId
  expect_str_end_match(log_contents[ind+2],'309') # before rm sent+target records to score
  expect_str_pattern_find(log_contents[ind+5], "96") # number of accounts to rescore
  expect_str_end_match(log_contents[ind+6],'184') # num of reocrds to score after rm and plus rescoring
  # check message a3RA0000000e0u5MAA
  ind <- grep('OPEN___a3RA0000000e486MAA',log_contents)
  expect_length(ind,1) # as this is expired message, so skip scoring half way
  expect_str_pattern_find(log_contents[ind+1],'expired') # detect msg is expired
  # check random sampling
  ind <- grep('Messaging strategy:',log_contents)
  expect_str_end_match(log_contents[ind],config[["LE_MS_newMessageStrategy"]])
  # check message a3RA0000000e0wBMAQ
  ind <- sum(grepl('a3RA0000000e0wBMAQ',log_contents))
  expect_equal(ind,0) # this message is not in message set, so will not be scoree this msg
  # check whether random sample calcuation is correct
  ind <- grep('a3RA0000000e47gMAA',log_contents)
  expect_str_end_match(log_contents[ind[1]],'lower bound 0.000845584853835724 upper bound 0.0780355582939658') # check calculation
  expect_str_pattern_find(log_contents[ind[1]+1],'expired') # check detected as expired
})


test_that("tests for nightly scoring with flagRescoring=FALSE", {
  # arguments for manual scoring
  isNightly <- TRUE
  flagRescoring <- FALSE
  buildUID <- runScoreModelMockDataReset()
  temp <- runScoreModel(isNightly, flagRescoring, buildUID)
  runUID <- temp[[1]]
  result_new <- temp[[2]]
  sc <- temp[[3]]

  # test cases
  # check calculation result is the same (as always use the same saved model and data)
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_manual_scores.RData', homedir))
  # load(sprintf('%s/messageSequence/tests/data/from_scoreModel_manual_messageRescoringTimesNew.RData', homedir))
  expect_equal(result_new[[1]],0)
  expect_equal(result_new[[2]],scores)
  expect_equal(result_new[[3]],NULL)
  
  # close spark
  closeSpark(sc)

  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # check isNightly and flagRescoring flag
  ind <- sum(grepl('flagRescoring=FALSE isNightly=TRUE', log_contents))
  expect_equal(ind>=1, TRUE)
})
