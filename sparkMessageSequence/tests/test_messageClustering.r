context('testing messageClustering script in sparkMSO module')
print(Sys.time())

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerprod_cs=c('Approved_Document_vod__c','RecordType'),pfizerusdev_learning=c('AKT_Message_Topic_Email_Learned'),pfizerusdev=c('Product')) # requiredMockDatalist in order of c('pfizerusdev','pfizerusdev_learning','pfizerusdev_stage','pfizerprod_cs')
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)

# load library required to run tests
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_cs <- getDBConnectionCS(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

# execute messageClustering script
source(sprintf("%s/messageSequence/messageClustering.r",homedir))
messageClustering(con_cs, con_l, con)

# get results from db
messageTopicEmailLearned <- dbGetQuery(con_l, 'SELECT * from AKT_Message_Topic_Email_Learned;')

# test case
test_that("test results has correct length of data", {
  expect_equal(dim(messageTopicEmailLearned),c(161,6))
})

test_that("test results has correct unique num of product, message, cluster", {
  expect_equal(unique(messageTopicEmailLearned$productUID),c("a00A0000000quNwIAI","a00A0000000quNsIAI"))
  expect_equal(length(unique(messageTopicEmailLearned$messageUID)),161)
  expect_equal(length(unique(messageTopicEmailLearned[messageTopicEmailLearned$productUID=="a00A0000000quNwIAI",5])),29)
  expect_equal(length(unique(messageTopicEmailLearned[messageTopicEmailLearned$productUID=="a00A0000000quNsIAI",5])),18)
})

# disconnect DB
dbDisconnect(con_cs)
dbDisconnect(con_l)