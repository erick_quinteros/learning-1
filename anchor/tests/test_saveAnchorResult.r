context('testing saveAnchorResult() in ANCHOR module')
print(Sys.time())

# load library and source script
library(data.table)
library(Learning.DataAccessLayer)
source(sprintf("%s/anchor/code/utils.r",homedir))
source(sprintf("%s/anchor/code/saveAnchorResult.r",homedir))

# set up mock data
requiredMockDataList <- list(pfizerusdev_learning=c('RepDateLocation','RepDateFacility'),pfizerusdev=c('RepDateLocation','RepDateFacility'))

# set up DB connection required for function
dataAccessLayer.common.initializeConnections(dbuser, dbpassword, dbhost, dbname, port)

# test for isNighlty=TRUE
test_that("test for save for nightly run", {
  # run saveAnchorResult
  isNightly <- TRUE
  RUN_UID <- "RUN_UID"
  BUILD_UID <- "BUILD_UID"
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_facility_DSE.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  resultList_DSE <- list(resultCentroid=result_centroid, resultFacility=result_facility_DSE)
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
  saveAnchorResult(isNightly, resultList_DSE, RUN_UID, BUILD_UID) 
  
  # run tests on DB result
  repDateLocation <- data.table(dbGetQuery(con, "SELECT * FROM RepDateLocation"))
  repDateLocation_l <- data.table(dbGetQuery(con_l, "SELECT * FROM RepDateLocation"))
  repDateFacility <- data.table(dbGetQuery(con, "SELECT * FROM RepDateFacility"))
  repDateFacility_l <- data.table(dbGetQuery(con_l, "SELECT * FROM RepDateFacility"))
  # test cases
  # test dimension
  expect_equal(dim(repDateLocation), c(15,9))
  expect_equal(dim(repDateLocation_l), c(0,13))
  expect_equal(dim(repDateFacility), c(391,9))
  expect_equal(dim(repDateFacility_l), c(0,13))
  # test entry in repDateLocation
  repDateLocation$date <- as.Date(repDateLocation$date)
  expect_equal(repDateLocation[order(repId,date),-c("learningRunUID","createdAt")], result_centroid[order(repId,date),-c("runDate","probability")])
  expect_equal(repDateLocation$learningRunUID[1],RUN_UID)
  # test entry in repDateFacility
  repDateFacility$date <- as.Date(repDateFacility$date)
  expect_equal(repDateFacility[order(repId,date,facilityId),-c("learningRunUID","createdAt")], result_facility_DSE[order(repId,date,facilityId),-c("runDate","accountId")])
  expect_equal(repDateFacility$learningRunUID[1],RUN_UID)
})

# test for isNightly=FALSE
test_that("test for save for manual run", {
  # run saveAnchorResult
  isNightly <- FALSE
  RUN_UID <- "RUN_UID"
  BUILD_UID <- "BUILD_UID"
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_facility.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  resultList <- list(resultCentroid=result_centroid, resultFacility=result_facility)
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
  saveAnchorResult(isNightly, resultList, RUN_UID, BUILD_UID)

  # run tests on DB result
  repDateLocation <- data.table(dbGetQuery(con, "SELECT * FROM RepDateLocation"))
  repDateLocation_l <- data.table(dbGetQuery(con_l, "SELECT * FROM RepDateLocation"))
  repDateFacility <- data.table(dbGetQuery(con, "SELECT * FROM RepDateFacility"))
  repDateFacility_l <- data.table(dbGetQuery(con_l, "SELECT * FROM RepDateFacility"))
  # test cases
  # test dimension
  expect_equal(dim(repDateLocation), c(0,9))
  expect_equal(dim(repDateLocation_l), c(15,13))
  expect_equal(dim(repDateFacility), c(0,9))
  expect_equal(dim(repDateFacility_l), c(463,13))
  # test entry in repDateLocation
  repDateLocation_l$date <- as.Date(repDateLocation_l$date)
  repDateLocation_l$runDate <- as.Date(repDateLocation_l$runDate)
  expect_equal(repDateLocation_l[order(repId,date),-c("learningRunUID","learningBuildUID","createdAt","updatedAt")], result_centroid[order(repId,date),])
  expect_equal(repDateLocation_l$learningRunUID[1],RUN_UID)
  expect_equal(repDateLocation_l$learningBuildUID[1],BUILD_UID)
  # test entry in repDateFacility
  repDateFacility_l$date <- as.Date(repDateFacility_l$date)
  repDateFacility_l$runDate <- as.Date(repDateFacility_l$runDate)
  repDateFacility_l[accountId==0,accountId:=NA]
  expect_equal(repDateFacility_l[order(repId,date,facilityId,accountId),-c("learningRunUID","learningBuildUID","createdAt","updatedAt")], result_facility[order(repId,date,facilityId,accountId),])
  expect_equal(repDateFacility_l$learningRunUID[1],RUN_UID)
  expect_equal(repDateFacility_l$learningBuildUID[1],BUILD_UID)
})

# close DB connection
dataAccessLayer.common.closeConnections()

