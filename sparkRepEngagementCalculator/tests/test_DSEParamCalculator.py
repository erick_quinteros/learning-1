import unittest
import sys
import os

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
module_dir = os.path.dirname(script_dir)
learning_dir = os.path.dirname(module_dir)
sys.path.append(learning_dir)

from common.unitTest.pyunittestUtils import unitTestParent
from sparkRepEngagementCalculator.DSEParamCalculator import *

class test_DSEParamCalculator(unitTestParent):

    # def test_a(self):
    #     print("hello dse")
    #     # print("hello, your args are " + str(sys.argv))

    def test_normal_round(self):
        normal_round(5.5, 100)

    def reqDBs(self):
        required_dse = [ "DSERun", "DSERunRepDate" ]
        required_cs = [  ]
        required_stage = [  ]
        required_learning = [  ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required

    def setUp(self):
        super().setUp()
        # self.dbconfig = DatabaseConfig.instance()
        # self.dbconfig.set_config(self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name, self.learning_db_name, self.cs_db_name, self.stage_db_name)
