import unittest
import sys
import os

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
module_dir = os.path.dirname(script_dir)
learning_dir = os.path.dirname(module_dir)
sys.path.append(learning_dir)

from common.unitTest.pyunittestUtils import unitTestParent

class test_SparkRepEngagementCalculator(unitTestParent):

    def test_a(self):
        # print("hello srec")
        print("hello, i am not main")

    def reqDBs(self):
        required_dse = [ "DSERun", "DSERunRepDate", "SparkDSERun", "SparkDSERunRepDate", "Rep", "Interaction"]
        required_cs = [ "Sync_Tracking_vod__c" ]
        required_stage = [ "AKT_RepLicense_arc", "rpt_dim_calendar", "RPT_Suggestion_Delivered_stg" ]
        required_learning = [ "RepEngagementCalculation", "ChannelActionMap" ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required

    def setUp(self):
        super().setUp()
        # self.dbconfig = DatabaseConfig.instance()
        # self.dbconfig.set_config(self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name, self.learning_db_name, self.cs_db_name, self.stage_db_name)


class test_SparkRepEngagementCalculatorMain(unitTestParent):

    def test_a(self):
        # print("hello srec")
        print("hello, i am main")

    def reqDBs(self):
        required_dse = [ "DSERun", "DSERunRepDate", "SparkDSERun", "SparkDSERunRepDate", "Rep", "Interaction"]
        required_cs = [ "Sync_Tracking_vod__c" ]
        required_stage = [ "AKT_RepLicense_arc", "rpt_dim_calendar", "RPT_Suggestion_Delivered_stg" ]
        required_learning = [ "RepEngagementCalculation", "ChannelActionMap" ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required

    def setUp(self):
        super().setUp()
        # self.dbconfig = DatabaseConfig.instance()
        # self.dbconfig.set_config(self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name, self.learning_db_name, self.cs_db_name, self.stage_db_name)
