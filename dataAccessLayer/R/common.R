# global connections
con <<- NULL
con_l <<- NULL
con_stage <<- NULL
con_cs <<- NULL

# connection functions
dataAccessLayer.common.initializeConnections <- function(dbuser, dbpassword, dbhost, dbname, port) {
  # open connection
  dataAccessLayer.common.openConnections(dbuser, dbpassword, dbhost, dbname, port)
  # register connection map between schema name & connection object
  dataAccessLayer.common.registerConnections()
  # register output names for modules (if it has this setting)
  if (!exists("anchorOutputTables", envir=globalenv())) { dataAccessLayer.anchor.registerOutputTables() }
}

dataAccessLayer.common.updateOutputTables <- function(runmodel) {
  flog.info("update output tables for %s for paraRun run", runmodel)
  eval(parse(text=sprintf("dataAccessLayer.%s.paraRun.updateOutputTables()", runmodel)))
}

dataAccessLayer.common.archiveOutput <- function(runmodel) {
  flog.info("archive output for %s for paraRun run", runmodel)
  eval(parse(text=sprintf("dataAccessLayer.%s.paraRun.archiveOutput()", runmodel)))
}

dataAccessLayer.common.openConnections <- function(dbuser, dbpassword, dbhost, dbname, port) {
  if (!exists("con", envir=globalenv())) {con <<- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)}
  if (!exists("con_l", envir=globalenv())) {con_l <<- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)}
  if (!exists("con_stage", envir=globalenv())) {con_stage <<- getDBConnectionStage(dbuser, dbpassword, dbhost, dbname, port)}
  if (!exists("con_cs", envir=globalenv())) {con_cs <<- getDBConnectionCS(dbuser, dbpassword, dbhost, dbname, port)}
  if (!exists("con_archive", envir=globalenv())) {con_archive <<- getDBConnectionArchive(dbuser, dbpassword, dbhost, dbname, port)}
}

dataAccessLayer.common.closeConnections <- function() {
  dbDisconnect(con)
  dbDisconnect(con_l)
  dbDisconnect(con_stage)
  dbDisconnect(con_cs) 
  dbDisconnect(con_archive) 
  rm(con, envir=globalenv())
  rm(con_l, envir=globalenv())
  rm(con_stage, envir=globalenv())
  rm(con_cs, envir=globalenv())
  rm(con_archive, envir=globalenv())
}

dataAccessLayer.common.registerConnections <- function() {
  conMap <<- list(dse="con", 
                  learning="con_l", 
                  stage="con_stage", 
                  cs="con_cs", 
                  archive="con_archive")
}

dataAccessLayer.common.getConnFromSchema <- function(schema) {
  conn <- eval(parse(text = conMap[[schema]]))
  return(conn)
}

dataAccessLayer.common.getSchemaNameFromConn <- function(conn) {
  schemaName <- dbGetInfo(conn)$dbname
  return(schemaName)
}

dataAccessLayer.common.getTableNameFromTableMap <- function(tableMap, tableNameKey) {
  tableName <- tableMap[[tableNameKey]][["tableName"]]
  return(tableName)
}

dataAccessLayer.common.getSchemaFromTableMap <- function(tableMap, tableNameKey) {
  schema <- tableMap[[tableNameKey]][["schema"]]
  return(schema)
}

dataAccessLayer.common.getArchiveTableName <- function(tableToArchive) {
  archiveTableName <- sprintf("%s_arc", tableToArchive)
  return(archiveTableName)
}

dataAccessLayer.common.getArchiveTableSchema <- function(tableToArchive) {
  archiveTableSchema <- "learning"
  return(archiveTableSchema)
}

# common functions
dataAccessLayer.common.truncate <- function(schema, tableName) {
  conn <- dataAccessLayer.common.getConnFromSchema(schema)
  dbClearResult(dbSendQuery(conn, sprintf("TRUNCATE TABLE %s", tableName)))
}

dataAccessLayer.common.deleteFrom <- function(schema, tableName, strFilterKeyValuePair) {
  conn <- dataAccessLayer.common.getConnFromSchema(schema)
  strConditionFilter <- paste(sapply(strFilterKeyValuePair, function(x){sprintf("%s='%s'", x[["key"]], x[["value"]])}), collapse=" AND ")
  SQL <- sprintf("DELETE FROM %s WHERE %s", tableName, strConditionFilter)
  dbClearResult(dbSendQuery(conn, SQL))
}

dataAccessLayer.common.write <- function(schema, tableName, dataToWrite) {
  conn <- dataAccessLayer.common.getConnFromSchema(schema)
  tryCatch(dbWriteTable(conn, name=tableName, value=as.data.frame(dataToWrite), overwrite=FALSE, append=TRUE, row.names=FALSE), error = function(e) {
    flog.error('Error in writing back to table %s in %s DB!: %s', tableName, schema, e)
    # quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
    stop(sprintf('Error in writing back to table %s in %s DB!', tableName, schema))
  })
}

dataAccessLayer.common.create <- function(schema, tableName, tableSchema) {
  conn <- dataAccessLayer.common.getConnFromSchema(schema)
  SQL <- sprintf("CREATE TABLE IF NOT EXISTS `%s` (%s) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;", tableName, tableSchema)
  dbClearResult(dbSendQuery(conn, SQL))
}

dataAccessLayer.common.createArchiveTableFor <- function(schema, tableName, archiveSchema=NULL, archiveTableName=NULL, archiveTableSchema=NULL) {
  if (is.null(archiveSchema)) { archiveSchema <- dataAccessLayer.common.getArchiveTableSchema(tableName) }
  if (is.null(archiveTableName)) { archiveTableName <- dataAccessLayer.common.getArchiveTableName(tableName) }
  # if provide provide archive table schema, create as usual table
  if (!is.null(archiveTableSchema)) { return(dataAccessLayer.common.create(archiveSchema, archiveTableName, archiveTableSchema)) }
  # else create using based on the table to archive
  conn <- dataAccessLayer.common.getConnFromSchema(archiveSchema)
  schemaName <- dataAccessLayer.common.getSchemaNameFromConn(conn)
  SQL <- sprintf("CREATE TABLE IF NOT EXISTS `%s` AS (SELECT *, CURRENT_DATE as arcDate FROM %s.%s WHERE false);", archiveTableName, schemaName, tableName)
  dbClearResult(dbSendQuery(conn, SQL))
}

dataAccessLayer.common.archive <- function(schema, tableName, archiveSchema=NULL, archiveTableName=NULL, colAsArcDate=NULL) {
  if (is.null(archiveSchema)) { archiveSchema <- dataAccessLayer.common.getArchiveTableSchema(tableName) }
  if (is.null(archiveTableName)) { archiveTableName <- dataAccessLayer.common.getArchiveTableName(tableName) }
  conn <- dataAccessLayer.common.getConnFromSchema(archiveSchema)
  schemaName <- dataAccessLayer.common.getSchemaNameFromConn(conn)
  colAsArcDate <- ifelse(is.null(colAsArcDate), sprintf("'%s'", as.character(Sys.Date())), colAsArcDate)
  SQL <- sprintf("INSERT INTO %s (SELECT *, %s as arcDate FROM %s.%s);", archiveTableName, colAsArcDate, schemaName, tableName)
  dbClearResult(dbSendQuery(conn, SQL))
}

dataAccessLayer.common.setNames.dse <- function() {
  dbGetQuery(con,"SET NAMES utf8")
}

dataAccessLayer.common.setNames.learning <- function() {
  dbGetQuery(con_l,"SET NAMES utf8")
}

dataAccessLayer.common.setNames.stage <- function() {
  dbGetQuery(con_stage,"SET NAMES utf8")
}

dataAccessLayer.common.setNames.cs <- function() {
  dbGetQuery(con_cs,"SET NAMES utf8")
}

dataAccessLayer.common.setNames.archive <- function() {
  dbGetQuery(con_archive,"SET NAMES utf8")
}

dataAccessLayer.common.write.runningLearningRun <- function(RUN_UID,BUILD_UID,versionUID,configUID,runType,now) {
  SQL <- sprintf("INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',1,'%s','running','%s');", RUN_UID,BUILD_UID,versionUID,configUID,runType,now)
  dbGetQuery(con_l, SQL)
}

dataAccessLayer.common.write.successLearningRun <- function(RUN_UID, now) {
  SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='success' WHERE learningRunUID='%s';",now,RUN_UID) 
  dbGetQuery(con_l, SQL)
}

dataAccessLayer.common.write.failureLearningRun <- function(RUN_UID, now) {
  SQL <- sprintf("UPDATE LearningRun SET executionDateTime='%s', executionStatus='failure' WHERE learningRunUID='%s';",now,RUN_UID) 
  dbGetQuery(con_l, SQL) 
}
