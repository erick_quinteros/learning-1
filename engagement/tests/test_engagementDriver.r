context('testing engagementDriver script in REM module')
print(Sys.time())

# load library and source script
library(properties)
library(uuid)
library(RMySQL)
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))

############################# func for run engegementDriver setup #############################################
runEngagementDriverMockDataSetup <- function (isNightly) {
  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('RepAccountEngagement','RepTeam'),pfizerusdev_learning=c('RepAccountEngagement','LearningRun','LearningBuild'))
  # learningBuild not required for run engagementDriver, just for testing
  requiredMockDataList_module <- list(pfizerusdev=c('Interaction','InteractionAccount','Account','Rep','RepAccountAssignment','RepTeamRep'),pfizerusdev_stage=c('Suggestion_Feedback_vod__c_arc','RecordType_vod__c_arc','Suggestion_vod__c_arc'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList,requiredMockDataList_module,'engagement')
  # set up unit test build folder and related configs
  # get buildUID
  buildUID <- readModuleConfig(homedir, 'engagement','buildUID')
  # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  if (!isNightly) {
    setupMockBuildDir(homedir, 'engagement', buildUID, files_to_copy='learning.properties', forNightly=FALSE)
  } else {
    setupMockBuildDir(homedir, 'engagement', buildUID, files_to_copy=NULL, forNightly=TRUE)
  }
  return (buildUID)
}

############################### func for run engagementDriver ########################################
runEngagementDriver <- function (isNightly, buildUID) {
  if (!isNightly) {
    # parameters needed for manual running
    RUN_UID <<- UUIDgenerate()
    BUILD_UID <<- buildUID
    propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,buildUID)
    config <- read.properties(propertiesFilePath)
    versionUID <- config[["versionUID"]]
    configUID  <- config[["configUID"]]
    con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    SQL <- sprintf("INSERT INTO LearningRun(learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',0,'REM','running','%s');",RUN_UID,buildUID,versionUID,configUID,now)
    dbClearResult(dbSendQuery(con_l, SQL))
    dbDisconnect(con_l)
  } else {
    if (exists("BUILD_UID", envir=globalenv())) {rm(BUILD_UID, envir=globalenv())}
  }
  # run engagementDriver script
  source(sprintf("%s/engagement/engagementDriver.r",homedir))
}

########################### main script for testing ########################################################
test_that('test for nightly running', {
  # run script
  isNightly <- TRUE
  buildUID <- runEngagementDriverMockDataSetup(isNightly)
  numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))
  runEngagementDriver(isNightly, buildUID)
  
  # run test cases
  # test build_dir has the correct structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_num_of_file(sprintf('%s/builds/%s',homedir,buildUID), numOfFilesInBuildFolder+2)
  # test entry in DB is fine
  # load data
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  repAccountEngagement <- dbGetQuery(con, 'SELECT * from RepAccountEngagement;')
  repAccountEngagement_l <- dbGetQuery(con_l, 'SELECT * from RepAccountEngagement;')
  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  learningBuild <- dbGetQuery(con_l, 'SELECT * from LearningBuild;')
  dbDisconnect(con_l)
  dbDisconnect(con)
  # check result entry
  expect_equal(dim(repAccountEngagement_l), c(0,10))
  expect_equal(dim(repAccountEngagement), c(33,10))
  load(sprintf('%s/engagement/tests/data/from_engagementDriver.RData', homedir))
  repAccountEngagement_mock[,c("repUID","accountUID")] <- list(as.integer(repAccountEngagement_mock$repUID), as.integer(repAccountEngagement_mock$accountUID))
  expect_equal(unname(repAccountEngagement[,c("repActionTypeId","repId","accountId","suggestionType","date","probability")]), unname(repAccountEngagement_mock[,c("repActionTypeId","repId","accountId","suggestionType","date","probability")]))
  # check learning run log entry
  expect_equal(dim(learningRun), c(1,10))
  expect_equal(dim(learningBuild), c(0,9))
  expect_equal(unname(unlist(learningRun[,c('isPublished','runType','executionStatus')])), c(1,'REM','success'))
  expect_equal(learningRun$updatedAt, learningRun$createdAt)
})

test_that('test for manual running', {
  # run script
  isNightly <- FALSE
  buildUID <- runEngagementDriverMockDataSetup(isNightly)
  runEngagementDriver(isNightly, buildUID)

  # run test cases
  # test build_dir has the correct structure
  expect_file_exists(sprintf('%s/builds/',homedir))
  expect_file_exists(sprintf('%s/builds/%s',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/learning.properties',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/log_Score_%s.txt',homedir,BUILD_UID,RUN_UID))
  expect_file_exists(sprintf('%s/builds/%s/print_Score_%s.txt',homedir,BUILD_UID,RUN_UID))
  # test entry in DB is fine
  # load data
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  repAccountEngagement <- dbGetQuery(con, 'SELECT * from RepAccountEngagement;')
  repAccountEngagement_l <- dbGetQuery(con_l, 'SELECT * from RepAccountEngagement;')
  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  learningBuild <- dbGetQuery(con_l, 'SELECT * from LearningBuild;')
  dbDisconnect(con_l)
  dbDisconnect(con)
  # check result entry in DB
  expect_equal(dim(repAccountEngagement_l), c(33,10))
  expect_equal(dim(repAccountEngagement), c(0,10))
  load(sprintf('%s/engagement/tests/data/from_engagementDriver_manual.RData', homedir))
  expect_equal(repAccountEngagement_l[,c("repActionTypeId","repUID","accountUID","suggestionType","date","probability")],repAccountEngagement_mock[,c("repActionTypeId","repUID","accountUID","suggestionType","date","probability")])
  # check learning run entry
  expect_equal(dim(learningRun), c(1,10))
  expect_equal(dim(learningBuild), c(0,9))
  expect_equal(unname(unlist(learningRun[,c('isPublished','runType','executionStatus')])), c(0,'REM','success'))
  expect_equal(learningRun$updatedAt>=learningRun$createdAt, TRUE)
})
