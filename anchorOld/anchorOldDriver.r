
##########################################################
#
# set up environment variables
#
##########################################################

library(futile.logger)

args <- commandArgs(TRUE)

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the expressions.
if(length(args)==0){
    print("No arguments supplied.")
    quit(save = "no", status = 1, runLast = FALSE)
}else{
    print("Arguments supplied.")
    for(i in 1:length(args)){
      eval(parse(text=args[[i]]))
      print(args[[i]]);
    }
}

dte <- Sys.Date()

flog.appender(appender.file(paste(logdir,"/anchor_",dte,".log",sep="")))
flog.threshold(INFO)

flog.info("Loading function library")
# load(sprintf("%s/AnchorLibrary.RData",libdir))
source(sprintf("%s/anchorOld/code/loadAnchorData.r",homedir))
source(sprintf("%s/anchorOld/code/calculateAnchor.r",homedir))
source(sprintf("%s/anchorOld/code/saveAnchorResult.r",homedir))

# flog.info("BuildDate = %s",as.character(anchorLibraryBuild[["buildDate"]]))

if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

interactions <- loadAnchorData(dbuser,dbpassword,dbhost,dbname,port,rundate)

result <- calculateAnchor()

saveAnchorResult(dbuser,dbpassword,dbhost,dbname,port,result)

flog.info("Done")

