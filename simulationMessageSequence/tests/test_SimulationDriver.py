import unittest

from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from simulationMessageSequence.SimulationMessageSequenceDriver import get_copy_storm_database_name
from simulationMessageSequence.SimulationMessageSequenceDriver import SimulationMessageSequenceDriver
from common.DataAccessLayer.DataAccessLayer import SegmentAccessor
from common.DataAccessLayer.DataAccessLayer import MessageAccessor


from os import path


import sys, os
_stderr = None
_stdout = None

def suppress_std_output():
    global _stderr
    global _stdout
    _stderr = sys.stderr
    _stdout = sys.stdout
    null = open(os.devnull, 'wb')
    sys.stdout = sys.stderr = null

def unsuppress_std_output():
    sys.stderr = _stderr
    sys.stdout = _stdout

# class MainFunctionTestCase(unittest.TestCase):
#
#     def test_less_argument_count(self):
#         assertRaises()
#
#     def test_additional_argument_count(self):

class MiscFunctionTestCase(unittest.TestCase):

    def test_get_copy_storm_database_name_sample(self):
        self.assertEqual(get_copy_storm_database_name("sample"), "sample_cs", "Invalid copy storm db name for sample")

    def test_get_copy_storm_database_name_pfizer(self):
        self.assertEqual(get_copy_storm_database_name("pfizerusprod"),"pfizerprod_cs", "Invalid copy storm db name for pfizer")

    def test_get_copy_storm_database_name_empty(self):
        self.assertEqual(get_copy_storm_database_name(""), "_cs", "Invalid copy storm db name for empty")


class SimulationDriverTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Create the parameters for the test
        db_host = "localhost"
        db_user = "root"
        db_password = "password"
        dse_db_name = "testpfizerus"
        learning_db_name = "testpfizerus_learning"
        cs_db_name = "testpfizerus_cs"
        db_port = 3306  # Update to 33066 for testing on Local machine
        mso_build_uid = "unit-test-mso-build"

        # Connect to the database on local
        # Get the singleton instance of the database config and set the properties
        database_config = DatabaseConfig.instance()
        database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name)

        # Create simulation tables

        # Creates

    def setUp(self):
        basepath = path.dirname(__file__)
        self.learning_home_dir = path.abspath(path.join(basepath, "..", ".."))

        self.test_mso_build_uid = "unit-test-mso-build"

    def __is_valid_simulation_run_uid(self, sim_run_uid):
        is_valid_sim_run_uid = True

        # Check if the run uid with build uid exists

        return is_valid_sim_run_uid

    def test_init_empty_values(self):
        self.assertRaises(AssertionError, SimulationMessageSequenceDriver, "", "")

    def test_init_invalid_learning_dir(self):
        self.assertRaises(AssertionError, SimulationMessageSequenceDriver, "/dsfd/ddd/@@@444/##", "")

    def test_init_invalid_mso_build_uid(self):
        self.assertRaises(AssertionError, SimulationMessageSequenceDriver, "./simulationMessageSequence/test/data", "")

    def test_init_valid_input(self):
        simulation_driver = SimulationMessageSequenceDriver(self.learning_home_dir, self.test_mso_build_uid)
        # Check if MSO directory
        self.assertEqual(simulation_driver.mso_build_dir, self.learning_home_dir + "/builds/" + self.test_mso_build_uid, "Incorrect MSO build directory")

        # Check learning home directory
        self.assertEqual(simulation_driver.learning_home_dir, self.learning_home_dir, "Incorrect learning home directory")

        # Check data base entry for learningRunUID
        sim_run_uid = simulation_driver.simulation_run_uid
        self.assertTrue(self.__is_valid_simulation_run_uid(sim_run_uid), "Simulation RunUID is invalid.")


class SimulationProcessTester(unittest.TestCase):

    def setUp(self):
        # Create the parameters for the test
        db_host = "localhost"
        db_user = "root"
        db_password = "password"
        dse_db_name = "testpfizerus"
        learning_db_name = "testpfizerus_learning"
        cs_db_name = "testpfizerus_cs"
        db_port = 3306  # Update to 33066 for testing on Local machine
        mso_build_uid = "unit-test-mso-build"

        # Get the singleton instance of the database config and set the properties
        database_config = DatabaseConfig.instance()
        database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name)

        basepath = path.dirname(__file__)
        self.learning_home_dir = path.abspath(path.join(basepath, "..", ".."))

        self.test_mso_build_uid = "unit-test-mso-build"

        self.simulation_driver = SimulationMessageSequenceDriver(self.learning_home_dir, self.test_mso_build_uid)
        self.sim_run_uid = self.simulation_driver.simulation_run_uid

    def test_generate_segments(self):
        SEGMENT_COUNT = 21
        SEGMENT_ACCOUNT_COUNT = 620

        self.simulation_driver.generate_segments_v2()

        segment_accessor = SegmentAccessor()
        generated_segment_count = segment_accessor.get_segment_count(self.test_mso_build_uid, self.sim_run_uid)

        self.assertEqual(generated_segment_count, SEGMENT_COUNT)

        generated_segment_account_count = segment_accessor.get_segment_account_count(self.test_mso_build_uid, self.sim_run_uid)
        self.assertEqual(generated_segment_account_count, SEGMENT_ACCOUNT_COUNT)

    def test_simulation(self):
        SEGMENT_COUNT = 21
        SEGMENT_ACCOUNT_COUNT = 620
        SIM_MSG_SEQ_COUNT = 144
        SIM_ACCOUNT_MSG_SEQ_COUNT = 310
        SIM_SEG_MSG_SEQ_COUNT = 21

        self.simulation_driver.start_simulation()

        segment_accessor = SegmentAccessor()
        generated_segment_count = segment_accessor.get_segment_count(self.test_mso_build_uid, self.sim_run_uid)

        self.assertEqual(generated_segment_count, SEGMENT_COUNT)

        generated_segment_account_count = segment_accessor.get_segment_account_count(self.test_mso_build_uid,
                                                                                     self.sim_run_uid)
        self.assertEqual(generated_segment_account_count, SEGMENT_ACCOUNT_COUNT)


        message_accessor = MessageAccessor()

        db_message_seq_count = message_accessor.get_message_seq_count(self.test_mso_build_uid, self.sim_run_uid)

        self.assertEqual(db_message_seq_count, SIM_MSG_SEQ_COUNT)

        db_account_message_seq_count = message_accessor.get_account_message_seq_count(self.test_mso_build_uid,
                                                                                     self.sim_run_uid)

        self.assertEqual(db_account_message_seq_count, SIM_ACCOUNT_MSG_SEQ_COUNT)

        db_segment_message_seq_count = message_accessor.get_segment_message_seq_count(self.test_mso_build_uid,
                                                                                      self.sim_run_uid)

        self.assertEqual(db_segment_message_seq_count, SIM_SEG_MSG_SEQ_COUNT)
