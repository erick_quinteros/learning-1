context('testing scoreModel.processInts, scoreModel.prepareDesignMatrix, scoreModel.getAcctsToReScore, scoreModel.scoreMessage, scoreModel.getScoresLowerUpperBound defined in scoreModel script in MSO module')
print(Sys.time())

# load library, loading common source scripts
library(h2o)
library(uuid)
library(Learning)
library(openxlsx)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
source(sprintf("%s/messageSequence/scoreModel.R",homedir))

# ####### test for scoreModel.processInts
# load required input
test_that("test scoreModel.processInts", {
  load(sprintf('%s/messageSequence/tests/data/from_whiteList_whiteListDB.RData', homedir)) # whiteListDB
  whiteList <- whiteListDB$accountId
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactions.RData', homedir))
  # run function
  ints_new <- scoreModel.processInts(whiteList, interactions)
  # run tests
  expect_equal(dim(ints_new), c(2098,9))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_processInts_ints.RData', homedir))
  expect_equal(ints_new, ints)
})

####### test for scoreModel.prepareDesignMatrix
test_that("test scoreModel.prepareDesignMatrix", {
  # load required input
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_AP.RData',homedir))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_processInts_ints.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  scoreDate <- as.Date(readModuleConfig(homedir, 'messageSequence','scoreDate')) # fix scoreDate so the result is always the same
  DAYS_CONSIDER_FOR_PRIORVISIT <- 30
  # run function
  allModel_new <- scoreModel.prepareDesignMatrix(AP, ints, interactionsP, scoreDate, DAYS_CONSIDER_FOR_PRIORVISIT)
  # run tests
  expect_equal(dim(allModel_new), c(309,754))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_prepareDesignMatrix_allModel.RData', homedir))
  expect_equal(allModel_new, allModel)
})

##### test for scoreModel.getAcctsToReScore
test_that("test scoreModel.getAcctsToReScore", {
  # required data from DB to get input
  requiredMockDataList <- list(pfizerusdev=c('MessageSet','MessageAlgorithm','MessageSetMessage'),pfizerusdev_learning=c('MessageRescoringTimes'))
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)
  # load required input
  tgtName <- 'a3RA00000001MtAMAU'
  msgId <- 703
  rescoringLimit <- readModuleConfig(homedir, 'messageSequence','rescoringLimit')
  percentageMessages <- readModuleConfig(homedir, 'messageSequence','percentageMessages')
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_messageSetMessage.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  intsSEND <- unique(interactionsP[productInteractionTypeName=="SEND" & !is.na(physicalMessageUID)][,c("accountId","physicalMessageUID","date")])
  ints_acct_msg_ldate <- intsSEND[, .(latestDate=max(date)), by=.(accountId,physicalMessageUID)]
  BUILD_UID <- readModuleConfig(homedir, 'messageSequence','buildUID')
  messageRescoringTimes <- data.table(dbGetQuery(con_l, sprintf("SELECT accountId, physicalMessageUID, messageId, learningBuildUID, rescoringTimes, createdAt, updatedAt FROM MessageRescoringTimes WHERE learningBuildUID='%s';",BUILD_UID)))
  modelsToScore <- data.table(dbGetQuery(con,"SELECT ms.messageSetId, ms.messageAlgorithmId, ma.externalId FROM MessageSet ms JOIN (SELECT messageAlgorithmId, externalId FROM MessageAlgorithm where isDeleted=0 and isActive=1 and messageAlgorithmType=1) ma ON ms.messageAlgorithmId=ma.messageAlgorithmId;"))
  msgAlgoId <- unique(modelsToScore$messageAlgorithmId)[1]
  msIds <- modelsToScore[messageAlgorithmId==msgAlgoId]$messageSetId 
  msgIds <- messageSetMessage[messageSetId %in% msIds]$messageId
  msm <- messageSetMessage[messageSetId %in% msIds]  # messageSetMessage filtering with msgAlgoId and those messageSetIds
  msmCnt <- msm[, .N, by="messageSetId"]             # group by messageSetId; count of messages in each messageSetId
  msm <- merge(msm, msmCnt)                          # tuple(messageSetId, messageId, Count) 
  msmMinCnt <- msm[, .(cnt=min(N)), by="messageId"] # tuple(messageId, minimum count)
  rm(interactionsP, modelsToScore, msgAlgoId, msIds, msgIds, msm, BUILD_UID)
  dbDisconnect(con)
  dbDisconnect(con_l)
  # run function
  accts_rescore_new <- scoreModel.getAcctsToReScore(ints_acct_msg_ldate, intsSEND, messageRescoringTimes, msmMinCnt, rescoringLimit, percentageMessages, tgtName, msgId)
  # run tests
  expect_length(accts_rescore_new, 96)
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_getAcctsToReScore_accts_rescore.RData', homedir))
  expect_equal(accts_rescore_new, accts_rescore)
})

# ##### test for scoreModel.scoreMessage
test_that("test scoreModel.scoreMessage", {
  # load required input
  BUILD_UID <- readModuleConfig(homedir, 'messageSequence','buildUID')
  setupMockBuildDir(homedir, 'messageSequence', BUILD_UID)
  targetName <- "OPEN___a3RA00000001MtAMAU"
  msgId <- 703
  modelsaveSpreadsheetName <- sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID)
  models <- data.table(read.xlsx(modelsaveSpreadsheetName,sheet=3))                         # models table contains the references to the models
  load(sprintf('%s/messageSequence/tests/data/for_scoreModel_scoreMessage_acctsToScore.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_prepareDesignMatrix_allModel.RData', homedir))
  t <- allModel
  sendName <- 'SEND___a3RA00000001MtAMAU'
  if(!sendName %in% names(t)) { t[,(sendName):=0] }
  if(!targetName %in% names(t)) {t[,(targetName):=0]}

  # start h2o which required for function
  suppressWarnings(h2o.init(nthreads=-1, max_mem_size="24g"))
  # run function
  t <- t[t$accountId %in% acctsToScore,]
  tp_new <- scoreModel.scoreMessage(t, targetName, models, msgId)
  # run tests
  expect_equal(dim(tp_new), c(88,7))
  load(sprintf('%s/messageSequence/tests/data/from_scoreModel_scoreMessage_tp.RData', homedir))
  expect_equal(tp_new, tp)
})

##### test fro scoreModel.getScoresLowerUpperBound
test_that("test scoreModel.getScoresLowerUpperBound", {
  # load required input
  load(sprintf('%s/messageSequence/tests/data/for_getScoresLowerUpperBound_scores.RData', homedir))
  # run function & tests
  tmp <- scoreModel.getScoresLowerUpperBound(scores, "Conservative")
  expect_equal(tmp, list(0.0021113096423771, 0.085927580395794))
  tmp <- scoreModel.getScoresLowerUpperBound(scores, "Moderate")
  expect_equal(tmp, list(0.044019445019086, 0.20017161726028))
  tmp <- scoreModel.getScoresLowerUpperBound(scores, "Aggressive")
  expect_equal(tmp, list(0.085927580395794, 0.31441565412477))
  tmp <- scoreModel.getScoresLowerUpperBound(scores, "Other_invalid_string")
  expect_equal(tmp, list(0.0021113096423771, 0.085927580395794))
})