context('testing messageSequence.prepareDynamicDesignMatrix defined in messageSequence script in MSO module')
print(Sys.time())

# source script contains the testing function
source(sprintf("%s/messageSequence/messageSequence.R",homedir))

# set required input
test_that("test messageSequence.prepareDynamicDesignMatrix", {
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactions.RData', homedir))
  load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_interactionsP.RData', homedir))
  targetNames <- c('a3RA00000001MtAMAU', 'a3RA0000000e0u5MAA', 'a3RA0000000e0wBMAQ', 'a3RA0000000e486MAA', 'a3RA0000000e47gMAA')
  DAYS_CONSIDER_FOR_PRIORVISIT <- 30
  # call funcs
  ints_new <- messageSequence.prepareDynamicDesignMatrix(interactions, interactionsP, targetNames, DAYS_CONSIDER_FOR_PRIORVISIT)
  # test cases
  expect_equal(dim(ints_new), c(482,11))
  load(sprintf('%s/messageSequence/tests/data/from_messageSequence_prepareDynamicDesignMatrix_ints.RData', homedir))
  setkey(ints_new,NULL)
  expect_equal(ints_new, ints)
})
