import logging
import sys

def create_logger(logger_name, log_file_path, log_level):

    logger = logging.getLogger(logger_name)
    std_handler = logging.StreamHandler(stream=sys.stdout)
    # file_handler = logging.FileHandler(log_file_path)
    formatter = logging.Formatter('%(asctime)s %(name)-5s %(levelname)-5s %(message)s')
    std_handler.setFormatter(formatter)
    # file_handler.setFormatter(formatter)
    logger.addHandler(std_handler)
    # logger.addHandler(file_handler)
    logger.setLevel(log_level)
    return logger
