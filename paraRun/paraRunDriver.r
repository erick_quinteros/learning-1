##########################################################
#
#
# aktana- paraRunNightlyDriver.
#
# description: driver used to run new learning module models parallel with the old one
#
# created by : learning-team-dev@aktana.com
#
# created on : 2019-07-22
#
# Copyright AKTANA (c) 2019.
#
#
##############################################################################################################
library(Learning)
library(Learning.DataAccessLayer)
library(futile.logger)


############################################
## Main program
############################################
# read args from command passed from bash script
args <- commandArgs(TRUE)
if(length(args)==0){
  print("No arguments supplied.")
  quit(save = "no", status = 1, runLast = FALSE)
}else{
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}
# config port param to be compatibale to be passed into dbConnect
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

# check whether it is nightly run
isNightly <- TRUE
if (exists("BUILD_UID")) { isNightly <- FALSE }

# open connections
dataAccessLayer.common.initializeConnections(dbuser, dbpassword, dbhost, dbname, port)
# create output tables & update its saved registered names for parallel run
dataAccessLayer.common.updateOutputTables(runmodel)

# run module to be run parallel
flog.info("Start running %s in parallel...", runmodel)
source(sprintf("%s/%s/%s.r",homedir, runmodel, rundriver), echo=TRUE)

# archiving the result if it is nightly run
if (isNightly) {
  dataAccessLayer.common.openConnections(dbuser, dbpassword, dbhost, dbname, port)
  dataAccessLayer.common.archiveOutput(runmodel)
  dataAccessLayer.common.closeConnections()
}