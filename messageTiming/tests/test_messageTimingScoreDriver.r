context('test the messageTimingScoreDriver script in the messageTiming module')
print(Sys.time())

# load library, loading common source scripts
library(data.table)
library(uuid)
library(openxlsx)
library(Learning)

source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))

# build used for testing
buildUID <- readModuleConfig(homedir, 'messageTiming','buildUID')
added_buildUID <- readModuleConfig(homedir, 'messageTiming','buildUID_nightlyscoreadd')
CATALOGENTRY <<- " "
BUILD_UID <<- buildUID

######################## func for reset mock data for run messageScoreDriver ###############################
runMessageTimingScoreDriverMockDataReset <- function(isNightly, buildUID) {
  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','Event','EventType','Interaction','InteractionType','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','RepActionType','Account','MessageSet','TimeToEngageAlgorithm','AccountTimeToEngage'),pfizerusdev_learning=c('LearningFile','LearningBuild','LearningRun', 'AccountTimeToEngage', 'AccountTimeToEngageReport','SegmentTimeToEngageReport','TTEhcpReport','TTEsegmentReport'),pfizerusdev_stage=c('AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'),pfizerprod_cs=c('Approved_Document_vod__c','RecordType', 'Call2_Sample_vod__c'))
  
  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)

  # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  setupMockBuildDir(homedir, 'messageTiming', buildUID, forNightly=isNightly, updateModelSaveFileSheetNo=list(2,4,6))
  
  # copy the build folder using new BUILD_UID to make sure the loop algorithm of nightly scoring works
  if (isNightly) {
    new_buildUID <- readModuleConfig(homedir, 'messageTiming','buildUID_nightlyscoreadd')
    new_versionUID <- readModuleConfig(homedir, 'messageTiming','versionUID_nightlyscoreadd')
    new_configUID <- readModuleConfig(homedir, 'messageTiming','configUID_nightlyscoreadd')
    setupMockBuildDir (homedir, 'messageTiming', buildUID, files_to_copy=NULL, forNightly=TRUE, new_BUILD_UID=new_buildUID, new_configUID=new_configUID, new_versionUID=new_versionUID, updateModelSaveFileSheetNo=list(2,4,6))
  }
}

######################## func for run messageScoreDriver ##########################################
runMessageTimingScoreDriver <- function(isNightly, buildUID) {
  # fix scoreDate so that the result is always the same
  scoreDate <<- as.Date(readModuleConfig(homedir, 'messageTiming','scoreDate'))

  if (!isNightly) {
    # arguments needed for manual scoring
    RUN_UID <<- UUIDgenerate()
    BUILD_UID <<- buildUID

    # add entry to learningRun (simulate what done by API before calling the R script) (for manual scoring only)
    config <- initializeConfigurationNew(homedir, BUILD_UID)
    con <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

    VERSION_UID <<- config[["versionUID"]]
    CONFIG_UID  <<- config[["configUID"]]

    SQL <- sprintf("INSERT INTO LearningRun (learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',0,'TTE','running','%s');",RUN_UID, BUILD_UID, VERSION_UID, CONFIG_UID, now)

    dbClearResult(dbSendQuery(con, SQL))
    dbDisconnect(con)
  } else {
    if (exists("BUILD_UID", envir=globalenv())) {rm(BUILD_UID, envir=globalenv())}
    if (exists("RUN_UID", envir=globalenv())) {rm(RUN_UID, envir=globalenv())}
  }

  # run messageScoreDriver
  source(sprintf('%s/messageTiming/messageTimingScoreDriver.r',homedir))

  return (RUN_UID)
}

############################# main test script ########################################################

######## test cases
#### test nightly scoring

test_that('test messageTimingScoreDriver for nighlty scoring', {
  # run script
  isNightly <- TRUE
  runmodel <<- " "

  runMessageTimingScoreDriverMockDataReset(isNightly, buildUID)

  numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))

  # Run score driver
  print("running messageTimingScoreDriver in nightly scoring ...")
  runUID <- runMessageTimingScoreDriver(isNightly, buildUID)
  print("Done messageTimingScoreDriver in nightly.")
  
  # load saved nightly scoring for comparison
  #load(sprintf('%s/messageTiming/tests/data/from_TTE_nightly_scores.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_DSE_AccountTimeToEngage.RData', homedir)) # scores_tte_dse
  #savedScores <- setkey(scores,NULL)
  
  # test entry in DB is fine
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  learningBuild <- dbGetQuery(con_l, 'SELECT * from LearningBuild;')
  accountTimeToEngage <- data.table(dbGetQuery(con, 'SELECT * FROM AccountTimeToEngage'))
  accountTimeToEngage[, predictionDate := as.Date(predictionDate)]

  dbDisconnect(con)
  dbDisconnect(con_l)

  expect_equal(dim(learningBuild), c(0,9))
  expect_equal(dim(learningRun), c(1,10))
  expect_setequal(unname(unlist(learningRun[,c('learningBuildUID','isPublished','runType','executionStatus')])), c(buildUID,1,'TTE','success'))
  # get runUID
  runUID <- learningRun[learningRun$learningBuildUID==buildUID,"learningRunUID"]
  runUID2 <- learningRun[learningRun$learningBuildUID==added_buildUID,"learningRunUID"]
  # test save resutls in DB
  expect_equal(dim(accountTimeToEngage), c(33600, 10))
  expect_length(unique(accountTimeToEngage$tteAlgorithmName), 1)
  expect_equal(dim(accountTimeToEngage[accountTimeToEngage$learningRunUID==runUID,]), c(33600, 10))

  # test the saved scores in DSE DB is the same as the one saved locally
  #expect_equal(unname(accountTimeToEngage[accountTimeToEngage$learningRunUID==runUID,c("accountId", "method")]), unname(data.frame(savedScores[,c("accountId", "method")])))
  scores_tte_dse[, c("learningRunUID", "runDate", "createdAt", "updatedAt") := NULL]
  accountTimeToEngage[, c("learningRunUID", "runDate", "createdAt", "updatedAt") := NULL]
  expect_equal(scores_tte_dse, accountTimeToEngage, ignore.col.order = T, check.attribute = F)
   
  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score",runUID,sep="_")))
  
  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # read config for result comparison
  propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,buildUID)
  config <- read.properties(propertiesFilePath)

  # check general log information
  ind <- grep('Result\\[\\[1\\]\\]= 0', log_contents)
  expect_gt(ind, 0)
  
  ind <- grep('entered saveTimingScoreResult function', log_contents)
  expect_gt(ind, 0)

  ind <- grep('Back from MT scoring and score save', log_contents)
  expect_gt(ind, 0)

})

#### test manual scoring
test_that('test messageScoreDriver for manual scoring', {
  # run script
  isNightly <- FALSE
  runmodel <<- " "

  runMessageTimingScoreDriverMockDataReset(isNightly, buildUID)
  
  print("running messageTimingScoreDriver in manual scoring ...")
  runUID <- runMessageTimingScoreDriver(isNightly, buildUID)
  print("Done messageTimingScoreDriver")
  
  # test cases
  # load saved manual scoring data for comparison
  #load(sprintf('%s/messageTiming/tests/data/from_TTE_manual_scores.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_LE_AccountTimeToEngage.RData', homedir)) # scores_tte_manual
  
  # test entry in DB is fine
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  
  accountTimeToEngage_l <- data.table(dbGetQuery(con_l, 'SELECT * from AccountTimeToEngage'))
  accountTimeToEngage_l[, predictionDate := as.Date(predictionDate)]
  accountTimeToEngage <- data.table(dbGetQuery(con, 'SELECT * from AccountTimeToEngage'))

  dbDisconnect(con)
  dbDisconnect(con_l)

  expect_equal(dim(learningRun), c(1,10))
  expect_equal(unname(unlist(learningRun[,c('learningRunUID','learningBuildUID','isPublished','runType','executionStatus')])), c(runUID,buildUID,0,'TTE','success'))
  expect_equal(learningRun$updatedAt>=learningRun$createdAt, TRUE)
  expect_equal(dim(accountTimeToEngage_l), c(33600, 8))
  expect_equal(dim(accountTimeToEngage), c(0, 10))
  
  # test the saved scores in Learning DB is the same as the one saved locally
  scores_tte_manual[, c("learningRunUID") := NULL]
  accountTimeToEngage_l[, c("learningRunUID", "createdAt", "updatedAt") := NULL]
  expect_equal(scores_tte_manual, accountTimeToEngage_l, ignore.col.order = T, check.attribute = F)

  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,buildUID,paste("Score", runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,buildUID,paste("Score", runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score", runUID,sep="_")))
  
  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score", runUID,sep="_"))

  # check general log information
  ind <- grep('Result\\[\\[1\\]\\]= 0', log_contents)
  expect_gt(ind, 0)
  
  ind <- grep('entered saveTimingScoreResult function', log_contents)
  expect_gt(ind, 0)

  ind <- grep('Back from MT scoring and score save', log_contents)
  expect_gt(ind, 0)

})

#### test TTE Reporting
test_that('test messageTimingScoreDriver for TTE Reporting ...', {
  # run script
  isNightly <- FALSE
  runmodel <<- "REPORT"

  runMessageTimingScoreDriverMockDataReset(isNightly, buildUID)
  
  print("running messageTimingScoreDriver for reporting ...")
  runUID <- runMessageTimingScoreDriver(isNightly, buildUID)
  print("Done messageTimingScoreDriver for reporting.")
  
  # test cases
  # load saved manual scoring data for comparison
  load(sprintf('%s/messageTiming/tests/data/from_TTE_scoresReport.RData', homedir)) # scores
  load(sprintf('%s/messageTiming/tests/data/from_TTE_scoresSegReport.RData', homedir)) # scoresSeg

  # test entry in DB is fine
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  
  accountTimeToEngage.rpt <- data.table(dbGetQuery(con_l, 'SELECT * from AccountTimeToEngageReport'))
  segmentTimeToEngage.rpt <- data.table(dbGetQuery(con_l, 'SELECT * from SegmentTimeToEngageReport'))

  dbDisconnect(con)
  dbDisconnect(con_l)

  expect_equal(dim(learningRun), c(1,10))
  expect_equal(unname(unlist(learningRun[,c('learningRunUID','learningBuildUID','isPublished','runType','executionStatus')])), c(runUID,buildUID,0,'TTE','success'))
  expect_equal(learningRun$updatedAt>=learningRun$createdAt, TRUE)
  expect_equal(dim(accountTimeToEngage.rpt), c(39200, 10))
  expect_equal(dim(segmentTimeToEngage.rpt), c(2870, 10))
  
  # test scores are the same as saved data
  scores[, c("learningRunUID", "runDate", "createdAt", "updatedAt") := NULL]
  accountTimeToEngage.rpt[, c("learningRunUID", "runDate", "createdAt", "updatedAt", "tteAlgorithmName") := NULL]
  expect_equal(scores, accountTimeToEngage.rpt, ignore.col.order = T, check.attribute = F)

  scoresSeg[, c("learningRunUID", "createdAt", "updatedAt") := NULL]
  segmentTimeToEngage.rpt[, c("learningRunUID", "createdAt", "updatedAt", "tteAlgorithmName") := NULL]
  expect_equal(scores, accountTimeToEngage.rpt, ignore.col.order = T, check.attribute = F)

  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score",runUID,sep="_")))
  
  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))

  # check general log information
  ind <- grep('Running reporting job', log_contents)
  expect_gt(ind, 0)
  
  ind <- grep('entered saveTimingScoreReport function', log_contents)
  expect_gt(ind, 0)

  ind <- grep('Back from MT scoring and score save', log_contents)
  expect_gt(ind, 0)
})
