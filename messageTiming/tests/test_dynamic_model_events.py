import pytest

import sys
sys.path.append("../")

import pandas as pd

from build_dynamic_model import build_dynamic_model

VAL_NO_DATA = 999 

def f():
    raise SystemExit(1)

def test_mytest():
    with pytest.raises(SystemExit):
        f()

def test_dynamic_model():
    """
    Usage: pytest -v -s  # print out input and output
    
    """
    
    df = pd.read_csv('./test_ints.csv')
    # df['date'] = df['date'].astype('datetime64[ns]')
    df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
    events = set(df['type'])

    print("\nInput data looks like:")
    print(df)

    design = build_dynamic_model(df)

    print("\nOutput model looks like:")
    print(design)

    design.to_csv("./test_ints_results.csv")

    for e in events:
        assert("pre" + e in design.columns)


