##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: database connection common function
##
## created by : shirley.xu@aktana.com
##
## created on : 2019-04-03
##
## Copyright AKTANA (c) 2019.
##
##
##########################################################

################################################
## function: get connection config for DSE DB
################################################
sparkGetDBConnection <- function(dbuser, dbpassword, dbhost, dbname=NULL, port=3306) {
  if (is.null(dbname)) { dbname <- ''}
  tz <- Sys.timezone()
  if (is.na(tz)) {tz <- "UTC"}
  connURL <- sprintf("jdbc:mysql://%s:%s/%s?user=%s&password=%s&serverTimezone=%s&useCompression=true&rewriteBatchedStatements=true&tinyInt1isBit=false", dbhost, port, dbname, dbuser, dbpassword, tz)
  return (connURL)
}

#################################################
## function: get connection config of learning DB
#################################################
sparkGetDBConnectionLearning <- function(dbuser, dbpassword, dbhost, dbname, port=3306)
{
  dbnameLearning <- sprintf("%s_learning", dbname)
  return(sparkGetDBConnection(dbuser, dbpassword, dbhost, dbnameLearning, port)) 
}

#################################################
## function: get connection handle of _stage DB
#################################################
sparkGetDBConnectionStage <- function(dbuser, dbpassword, dbhost, dbname, port=3306)
{
  dbname_stage <- sprintf("%s_stage", dbname)
  return(sparkGetDBConnection(dbuser, dbpassword, dbhost, dbname_stage, port)) 
}

#################################################
## function: get connection handle of _cs DB
#################################################
sparkGetDBConnectionCS <- function(dbuser, dbpassword, dbhost, dbname, port=3306)
{
  dbname_cs <- paste0(dbname,"_cs")
  if(substr(dbname,1,8)=="pfizerus") dbname_cs <- "pfizerprod_cs"  # this is exception for pfizerus (from messageClustering)
  return(sparkGetDBConnection(dbuser, dbpassword, dbhost, dbname_cs, port)) 
}

################################################
## function: read data from DB using jdbc for spark
################################################
sparkReadFromDB <- function(sc, sparkDBconURL, sql, name=NULL, numPartitions="2", partitionColumn=NULL, lowerBound="1", upperBound="100000000", memory=FALSE, repartition=48)
{
  readQuery <- sprintf("(%s) as my_query", sql)
  if (is.null(name)) { name <- paste("fromsql", stri_rand_strings(1, 10), sep = "_")}
  if (numPartitions==1 | is.null(partitionColumn)) {
    return (spark_read_jdbc(sc, name,  
                            options = list(url = sparkDBconURL, driver='com.mysql.jdbc.Driver', dbtable = readQuery), 
                            memory=memory, 
                            overwrite=TRUE, 
                            repartition=repartition))
  } else {
    return (spark_read_jdbc(sc, name,  
                            options = list(url = sparkDBconURL, driver='com.mysql.jdbc.Driver', dbtable = readQuery, numPartitions=numPartitions, partitionColumn=partitionColumn, lowerBound=lowerBound, upperBound=upperBound), 
                            memory=memory, 
                            overwrite=TRUE, 
                            repartition=repartition))
  }
}

################################################
## function: write spark data to DB using jdbc for spark
################################################
sparkWriteToDB <- function(sparkDBconURL, data, tableName, ifExists='append', numPartitions="2")
{
  spark_write_jdbc(data, name = tableName,
                   options = list(url = sparkDBconURL, driver='com.mysql.jdbc.Driver', numPartitions=numPartitions), 
                   mode = ifExists)
  flog.info("finish save to DB")
}