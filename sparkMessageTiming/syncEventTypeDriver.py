# Update python path to find DataAccessLayer
import os
import sys

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)
#import packages
from common.DataAccessLayer.DataAccessLayer import MessageAccessor
from common.DataAccessLayer.DataAccessLayer import SegmentAccessor
from common.DataAccessLayer.DataAccessLayer import DatabaseIntializer
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.DataAccessLayer.DataAccessLayer import LearningAccessor
from common.DataAccessLayer.DataAccessLayer import DSEAccessor
import itertools
import datetime
from common.logger.Logger import create_logger
import common.DataAccessLayer.DataAccessLayer as data_access_layer
import logging
import pandas as pd
import time
# import threading

LEARNING_DB_SUFFIX = "_learning"
CS_DB_SUFFIX = "_cs"
logger = None

class EventTypeSyncDriver:
    
    def __init__(self, learning_home_dir):
        self.learning_home_dir = learning_home_dir
        self.__intialize_logger()


    def __intialize_logger(self):
        """

        :return:
        """
        # Create logger path
        timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        log_file_path = self.learning_home_dir + "/../logs/syncEventTypeDriver." + timestamp + ".stdout"

        # Crete logger with file and output
        global logger
        logger_name = "syncEventTypeDriver"
        logger = create_logger(logger_name, log_file_path, logging.DEBUG)

        # Initialize data access layer logger
        data_access_layer.initialize_logger(logger_name)

    def synchronize(self, customer_name):

        # Get the Table EventType from dse and filter
        dse_accessor = DSEAccessor()
        eventType_df = dse_accessor.get_eventType_df(customer_name)
        logger.info("Get EventType table from DSE table")
        

        # Write to learning table
        if eventType_df.empty:
        	logger.error("Error! No EventType selected!")
        learning_accessor = LearningAccessor()
        learning_accessor.write_eventType_df(eventType_df)
        # event_type_df.to_sql("")


def get_copy_storm_database_name(dse_db_name):
    """
    This function returns the copy storm database name based on the logic and customer exceptions.
    :param dse_db_name:
    :return:
    """
    cs_db_name =  dse_db_name + CS_DB_SUFFIX

    if cs_db_name[:8] == "pfizerus":
        cs_db_name = 'pfizerprod_cs'

    return cs_db_name


def main():
    """
    This is main function
    """
    # Validate the input argument to the script
    
    # Retrieve the arguments
    input_args_dict = dict(i.split("=") for i in sys.argv[1:])
    try:
        db_host = input_args_dict['dbhost']
        db_user = input_args_dict['dbuser']
        db_password = input_args_dict['dbpassword']
        dse_db_name = input_args_dict['dbname']
        db_port = input_args_dict['port']          # Update to 33066 for testing on Local machine
        # db_port = 33066
        customer_name = input_args_dict['customer']
        learning_home_dir = input_args_dict['homedir']
    except KeyError as e:
        print("Could not get parameters:{}".format(e))
        return

    # Create Learning database name
    learning_db_name = dse_db_name + LEARNING_DB_SUFFIX
    cs_db_name = get_copy_storm_database_name(dse_db_name)


    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name)

    eventSyncDriver = EventTypeSyncDriver(learning_home_dir)
    eventSyncDriver.synchronize(customer_name)


if __name__ == "__main__":
    main()
