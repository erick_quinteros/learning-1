import numpy as np
import pandas as pd
from datetime import datetime

from anchorAccuracy.utils.math_helper_func import intersection_area, haversine_numpy, get_max_distance_between_n_points, reject_outliers
from anchorAccuracy.utils.groupby_calculation_helper_func import calculate_basic_metrics_visit_predict, calculate_predict_coverage, calculate_percentage_visits_in_overlap, calculate_suggesstion_in_predict_circle, prepare_set_tuple, calculate_max_travel_over_max_near_distance, calculate_max_daily_travel, calculate_max_near_distance
from common.pyUtils.logger import get_module_logger

logger = get_module_logger(__name__)


class AccuracyReportCalculator:
    """
    object used to do the mainly calculation of anchor accuracy analysis
    """

    def __init__(self, isNightly, predict_df, visit_df, suggest_df, predict_facility_df=None):
        self.is_nightly = isNightly
        self.predicted = predict_df
        self.visited = visit_df
        self.suggested = suggest_df
        self.predicted_facility = predict_facility_df
        self.result = None

    def _merge_predict_visit(self):
        # merge predicted and visited
        logger.info('Merge visited and predicted....')
        self.result = pd.merge(left=self.visited, right=self.predicted, on=['repId', 'date'], how='outer',sort=False)
        return self

    def _calculate_basic_metrics_for_visit_predict(self):
        self.result = self.result.groupby(['repId', 'date']).apply(calculate_basic_metrics_visit_predict).reset_index()
        logger.info('finish calculating radius, centroid, max daily travel distance of visit, and number of visits and number of visit in visit-predict overlap, and predictedVistedMSE')
        return self

    def _get_further_processed_metrics_for_visit_predict(self):
        # add the columns needs to be filled
        self.result = self.result.reindex(columns=self.result.columns.tolist() + ['predictedVisitedDiffKm','predictedVisitedDiffLessThanMaxTravel','visitAreaKm2','overlapAreaKm2','pctOverlapCoverage','pctVisitInOverlap','aggPctOverlapCoveragePctVisitInOverlap'])
        # calculate
        self.result.loc[:, 'predictedVisitedDiffKm'] = haversine_numpy(self.result.loc[:,['predict_latitude', 'predict_longitude','centroid_latitude','centroid_longitude']].values)
        self.result.loc[:, 'visitAreaKm2'] = self.result.loc[:, 'visitRadiusKm'] ** 2 * np.pi
        self.result.loc[:, 'overlapAreaKm2'] = self.result.apply(lambda x: intersection_area(x.loc['predictedVisitedDiffKm'], x.loc['visitRadiusKm']), axis=1)
        self.result.loc[:, 'pctOverlapCoverage'] = self.result.apply(calculate_predict_coverage, axis=1)
        self.result.loc[:, 'pctVisitInOverlap'] = self.result.apply(calculate_percentage_visits_in_overlap,axis=1)
        self.result.loc[:, 'predictedVisitedDiffLessThanMaxTravel'] = np.nan
        self.result.loc[self.result.loc[:, 'predictedVisitedDiffKm'] <= self.result.loc[:,'maxDayTravelKm'], 'predictedVisitedDiffLessThanMaxTravel'] = 1
        self.result.loc[self.result.loc[:, 'predictedVisitedDiffKm'] > self.result.loc[:,'maxDayTravelKm'], 'predictedVisitedDiffLessThanMaxTravel'] = 0
        self.result.loc[:, 'aggPctOverlapCoveragePctVisitInOverlap'] = 0.3 * self.result.loc[:,'pctOverlapCoverage'] + 0.7 * self.result.loc[:,'pctVisitInOverlap']
        logger.info('finish calculating the percentage of predict coverage, percentage of visited covered by predict area, and their aggregated metrics aggPctOverlapCoveragePctVisitInOverlap')
        return self

    def calculate_metrics_comparing_visit_predict(self):
        logger.info('Start Calculating the accuracy metrics comparing predict and visit data...')
        self._merge_predict_visit()
        if self.result.shape[0] > 0:
            self._calculate_basic_metrics_for_visit_predict()._get_further_processed_metrics_for_visit_predict()
        else:
            logger.warn("loaded predict & visit data are both empty")
            self.result = pd.DataFrame(columns=['predict_latitude', 'predict_longitude', 'centroid_latitude', 'centroid_longitude', 'numOfVisits',
               'numOfVisitsInOverlap', 'maxDayTravelKm', 'visitRadiusKm', 'maxNearDistanceKm', 'predictedVisitedMSE','predictedVisitedDiffKm','predictedVisitedDiffLessThanMaxTravel','visitAreaKm2','overlapAreaKm2','pctOverlapCoverage','pctVisitInOverlap','aggPctOverlapCoveragePctVisitInOverlap'])
        return self

    def _calculate_metrics_comparing_suggest_predict(self):
        # merge suggest and predict
        logger.info('Merge suggest with processed analysis resultfor further analysis...')
        suggested = self.suggested.merge(self.result.loc[:, ['repId', 'date', 'predict_latitude', 'predict_longitude', 'visitRadiusKm']], on=['repId', 'date'], how='left', sort=False)

        # calculate num of suggestion and num of suggestions within the prediction circle
        suggested_grouped = suggested.groupby(['repId', 'date']).apply(calculate_suggesstion_in_predict_circle).reset_index()
        logger.info('finish counting how many and how many suggestions are in predict circle')

        # merge suggested_grouped to visited_predicted results
        # record max date before merge for visited_predicted
        max_visited_predicted_record_date = max(self.result.date.copy())
        # merge
        self.result = self.result.merge(suggested_grouped, on=['repId', 'date'], how='outer', sort=False)
        # keep date only to max_visited_predicted_record_date
        self.result = self.result.loc[self.result.date <= max_visited_predicted_record_date, :]

        # fill na in numOfSugs and numOfVisits to 0
        self.result.loc[pd.isnull(self.result.numOfSugs), 'numOfSugs'] = 0
        self.result.loc[pd.isnull(self.result.numOfVisits), 'numOfVisits'] = 0
        logger.info('finish merging num of suggestion, num of suggestion in predict circle to processed analysis result')
        return self

    def _calculate_metrics_comparing_suggest_visit(self):
        # get visit & suggest visit accountId for every rep every day
        visited = self.visited.loc[:, ['repId', 'date', 'accountId']].drop_duplicates()
        visited['match_visit'] = 1
        suggested = self.suggested.loc[:, ['repId', 'date', 'accountId']]

        # calculate number of suggested visited
        suggested_visited = suggested.merge(visited, on=['repId','date','accountId'], how='left', sort=False)
        suggested_visited_grouped = suggested_visited.loc[:,['repId','date','match_visit']].groupby(['repId','date']).count().reset_index().rename(columns={'match_visit':'numOfSugsTaken'})
        logger.info('finish checking how many suggestions are taken based on latitude and longitude')

        # merge to combined visited_predicted_grouped
        self.result = self.result.merge(suggested_visited_grouped, on=['repId', 'date'], how='left', sort=False)
        logger.info('finish merging how many suggestions are taken to the process analysis reuslt')
        return self

    def _get_further_processed_metrics_suggest(self):
        self.result = self.result.reindex(columns=self.result.columns.tolist() + ['pctSugsTaken', 'pctSugsInPredict'])
        self.result.loc[:, 'pctSugsTaken'] = self.result.apply(lambda x: x.loc['numOfSugsTaken'] / x.loc['numOfSugs'] if x.loc['numOfSugs'] != 0 else np.nan, axis=1)
        self.result.loc[:, 'pctSugsInPredict'] = self.result.apply(lambda x: x.loc['numOfSugsInPredict'] / x.loc['numOfSugs'] if x.loc['numOfSugs'] != 0 else np.nan, axis=1)
        logger.info('finish calculating percentage of sugs taken and in predict circle')
        return self

    def calculate_metrics_comparing_suggest_with_predict_visit(self):
        logger.info('Start calculating the accuracy metrics comparing suggest with predict and visit...')
        if self.suggested.shape[0] > 0:
            self._calculate_metrics_comparing_suggest_predict()._calculate_metrics_comparing_suggest_visit()
        else:
            logger.warn("loaded suggest data is empty")
            self.result = self.result.reindex(columns=self.result.columns.tolist() + ['numOfSugsTaken', 'numOfSugs', 'numOfSugsInPredict'])
            self.result.loc[:, 'numOfSugs'] = 0
        self._get_further_processed_metrics_suggest()
        return self

    def calculate_metrics_comparing_predicit_visit_facility(self):
        # if self.is_nightly: # only for nightly
        if self.predicted_facility.shape[0] > 0:
            # merge to get predicted facility visited
            visited = self.visited.loc[:,['repId','date','facilityId']].drop_duplicates()
            visited['match_visit'] = 1
            predicted_facility_visited = self.predicted_facility.merge(visited, on=['repId', 'date', 'facilityId'],
                                                                       how='left', sort=False)
            # prepare to calculated weighted pct facility visited
            predicted_facility_visited['probability_visit'] = predicted_facility_visited['probability']
            predicted_facility_visited.loc[
                pd.isnull(predicted_facility_visited['match_visit']), 'probability_visit'] = np.nan
            # calculated weighted pct facility visited
            predicted_facility_visited_grouped = predicted_facility_visited.groupby(['repId', 'date']).agg(
                {'probability': np.nansum, 'probability_visit': np.nansum}).reset_index().rename(
                columns={'probability_visit': 'numOfFacilityPredictedVisited', 'probability': 'numOfFacilityPredicted'})
            predicted_facility_visited_grouped['pctPredictedFacilityVisited'] = predicted_facility_visited_grouped['numOfFacilityPredictedVisited'] / predicted_facility_visited_grouped['numOfFacilityPredicted']
            self.result = self.result.merge(predicted_facility_visited_grouped, on=['repId', 'date'], how='left', sort=False)
            # fill NA with O, etc
            self.result.loc[pd.isnull(self.result.numOfFacilityPredicted), 'numOfFacilityPredicted'] = 0
            self.result.loc[(self.result.numOfFacilityPredicted==0) & (self.result.numOfVisits>0), ['numOfFacilityPredictedVisited','pctPredictedFacilityVisited']] = 0
        else:
            logger.warn('loaded predict facility data is empty')
            self.result = self.result.reindex(columns=self.result.columns.tolist() + ['numOfFacilityPredicted', 'numOfFacilityPredictedVisited','pctPredictedFacilityVisited'])
        # else:
        #     self.result = self.result.reindex(columns=self.result.columns.tolist() + ['numOfFacilityPredicted', 'numOfFacilityPredictedVisited','pctPredictedFacilityVisited'])
        logger.info('Finish calculating facilicty based matrix')
        return self

    def add_missing_data_flag(self):
        self.result = self.result.reindex(columns=self.result.columns.tolist() + ['hasVisited', 'hasPredicted', 'hasSuggested'])
        self.result.loc[pd.isnull(pd.isnull(self.result.centroid_latitude) | pd.isnull(self.result.centroid_longitude)), 'numOfVisits'] = 0
        self.result.loc[:, ['hasVisited', 'hasPredicted', 'hasSuggested']] = 1
        self.result.loc[self.result.numOfVisits == 0, 'hasVisited'] = 0
        self.result.loc[pd.isnull(self.result.predict_latitude) | pd.isnull(self.result.predict_longitude), 'hasPredicted'] = 0
        self.result.loc[self.result.numOfSugs == 0, 'hasSuggested'] = 0
        logger.info('Finish adding no data label')
        return self

    def calculate_maxDayTravelKm_over_maxNearDistance(self):
        # calculate maxNearDistanceKm first for history run
        if not self.is_nightly:
            self.result.drop(axis=1, columns='maxNearDistanceKm', inplace=True)
            max_near_distance = self.result.loc[:, ['repId', 'date', 'maxDayTravelKm']].groupby(
                'repId').apply(calculate_max_near_distance).reset_index(name='maxNearDistanceKm')
            self.result = self.result.merge(max_near_distance, on='repId', how='left', sort=False)

        self.result = self.result.reindex(columns=self.result.columns.tolist() + ['maxDayTravelOverMaxNear'])
        self.result.loc[:, 'maxDayTravelOverMaxNear'] = self.result.loc[:, ['maxDayTravelKm','maxNearDistanceKm']].apply(calculate_max_travel_over_max_near_distance,axis=1)
        logger.info('Finish calcualting maxDayTravelKm over maxNearDistanceKm')
        return self

    def run(self):
        return self.calculate_metrics_comparing_visit_predict().calculate_metrics_comparing_suggest_with_predict_visit().calculate_metrics_comparing_predicit_visit_facility().add_missing_data_flag().calculate_maxDayTravelKm_over_maxNearDistance()
