context('testing calculateEngagement() func in spark REM module')
print(Sys.time())

# load library and source script
library(properties)
library(futile.logger)
library(uuid)
source(sprintf("%s/sparkEngagement/code/utils.R",homedir))
source(sprintf("%s/sparkEngagement/code/calculateEngagement.R",homedir))
# required to run tests
library(sparkLearning)
source(sprintf('%s/common/unitTest/utilsFuncSpark.r',homedir))

# set up unit test build folder and related configs
# get buildUID
BUILD_UID <<- readModuleConfig(homedir, 'engagement','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'engagement', BUILD_UID, files_to_copy='learning.properties')

# parameters needed to run func
if(!exists("RUN_UID")) RUN_UID <<- UUIDgenerate()
# parameters from config
propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,BUILD_UID)
config <- read.properties(propertiesFilePath)
today <<- min(as.Date(config[["LE_RE_today"]]), Sys.Date())-1
sugStartDate <<- today-as.numeric(config[["LE_RE_sugStartDate"]]) # how far back from today to read the data
lookForward <<- max(1, as.numeric(config[["LE_RE_lookForward"]]))# how far forward from today to predict
engageWindow <<- max(lookForward, as.numeric(config[["LE_RE_EngageWindow"]]))  # how far forward to look for engagements for the suggestions that are ignored
epsilon <<- as.numeric(config[["LE_RE_epsilon"]])                 # for numerical equivalence to zero
numberCores <<- as.numeric(config[["LE_RE_numberCores"]])         # number of cores to use
channels <<- strsplit(config[["LE_RE_channels"]],split=";")[[1]]  # minor parsing of the channel parameter
includeReactions <<- strsplit(config[["LE_RE_includeReactions"]], split=";")[[1]]  # minor parsing of which reactions to include
useForProbability <<- config[["LE_RE_useForProbability"]]    # parsing of useForProbability
# from saved data from laodEngagementData
load(sprintf('%s/engagement/tests/data/from_loadEngagementData.RData', homedir))
interactions <- data[[1]]
suggestions <- data[[2]]
channels <- channels[channels %in% unique(suggestions$repActionTypeId)]
chl <- channels[1]

# spark setting
library(sparkLearning)
library(sparklyr)
library(dplyr)
sc <- initializeSpark(homedir)
interactions$date <- as.character(interactions$date)
interactions <- sdf_copy_to(sc, interactions, "interactions", overwrite=TRUE, memory=FALSE)
interactions <- interactions %>% mutate(date = to_date(date))
suggestions$date <- as.character(suggestions$date)
suggestions <- sdf_copy_to(sc, suggestions, "suggestions", overwrite=TRUE, memory=FALSE)
suggestions <- suggestions %>% mutate(date = to_date(date))

# run script
result_new <- calculateEngagement(sc, chl, useForProbability, interactions, suggestions)

# test case
test_that("test have correct length of data", {
  expect_equal(sdf_dim(result_new),c(15,8))
})

test_that("test result is the same as the one saved ", {
  load(sprintf('%s/engagement/tests/data/from_calculateEngagement.RData', homedir))
  result_new <- convertSDFToDF(result_new)
  expect_equal(result_new[order(repUID,accountUID,date),-c("learningRunUID")], result[order(repUID,accountUID,date),-c("learningRunUID")])
})

# disconnect spark
closeSpark(sc)