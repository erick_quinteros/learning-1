\documentclass[10pt]{article}
\usepackage[letterpaper, margin=1in]{geometry}
\usepackage[usenames]{color} %used for font color
\usepackage{amssymb} %maths
\usepackage{amsmath} %maths
\usepackage[utf8]{inputenc} %useful to type directly diacritic characters
\usepackage{graphicx}
\begin{document}
\setlength{\parindent}{0pt}
\parskip = \baselineskip
\section*{Documentation of Message Sequence Learning}
\em{Marc Cohen} \\
\em{March 2017}

This is a technical document that describes the underlying logic and approach to message sequence predictions.
It serves as documentation to the current implementation of the code.

\section*{Introduction}
The objective is to provide a decision model that can be used to identify the next best message to send to an individual HCP based on
\begin{itemize}
\item who that HCP demographic and/or account characteristic data (covariates) and
\item how that HCP behaves - the history of HCP actions.
\end{itemize}
The approach taken is to use machine learning (ML) algorithms to estimate a model of HCP likelihood of action based on the historical behaviors for the population of HCPs
and then use that model to score each individual HCP on a real-time basis to identify the next best message given the individual's past message behavior.
For the Message Sequence use case the target of the model is either email message opens or clicks.
The objective of the next best action recommended by the model is to maximize the likelihood that the HCP will open or click on a message.
The modular structure of the learning platform supports other use cases with other targets so while much of the discussion below is applicable to
other use cases the main focus here describes the details for this specific use case.

This document describes the overall architecture of the approach and how it is integrated into the DSE execution environment.
The details of the DSE integration and implementation are not covered.
The details leading up to the DSE integration are documented here.

First an overview of the architectural components with a focus on the learning components is discussed.
Then details on model construction and the ML algorithms used to estimate model parameters is provided.
Then some of the measures of model performance that are included in the output are discussed.
Finally, the specific input parameters and output tables are discussed.

\section*{Overview}

The learning platform consists of three major components shown in the the diagram below.
These components are automated model build, the actual model itself, and the scoring calculation.
\begin{description}
\item[Automated model builds] this component does most of the heavy lifting. Including the following steps:
  \begin{itemize}
  \item reads the data from the data stores (currently the DSE DBs but soon to be BDP).
  \item builds the features from the raw data that will be used in the construction of the design matrix.
  \item builds the design matrix.
  \item launches the ML algorithms and stores the models on the sever and performance metrics in the DSE DB.
  \end{itemize}
\item[Models] are the models produced by the previous step.
  These are stored on the execution server and packaged with configuration information used to build them and the performance metrics.
  They are persistent and can be used either directly by the DSE or by the scoring engine for decisioning on what the specific next best message is given
  the current history of individual HCP behaviors.
\item[Scores] are the output from the scoring engine. Typically, these are produced nightly and stored in the DSE DB for consumption by DSE in deciding what the next message should be.
\end{description}

The following figure shows the overall architectural and how the learning components are integrated with the overall Aktana architecture.
Of course, a lot of detail is missing from the diagram but it shows the overall flow of data and informatin between components.
\begin{center}
\includegraphics[width=6in]{Architecture.png}
\end{center}
One important detail that is missing from the diagram regards model management and some of control mechanisms that impact learning code operation.
The automated model build component reads a configuration file (or table from the DSE DB) that is used to control some aspects
of the ML algorithm and identify variables to be included in the design matrix and in the contruction of the features to be included in the design matrix.
The details on configuration parameters are given in a later section.
The models produced by this component, performance documentation, execution logs, and the configuration used are packaged into a directory and saved as an artifact on the execution server.
The package is defined by a directory that has a time stamp as part of the directory name.
That time stamp is included with each of the artifact components saved in the directory so that there is an audit trail associated with the model build.
All relevant components are saved except for the data used to build the model.
Some of this information is replicated in the DSE DB however subsequent runs of the model build do not preserve that information in the DSE DB; it will be lost.
However, that information will be preserved on the server until it is actively deleted.

\section*{Automated Model Build}

This section goes into the details of the model build including the model formulation, the scoring, and the structure of the server artifacts.
Before discussing the details of the automated model build it is good to have an overview of the design matrix.
This component built in the automated model build is a key aspect of the learning platform and is tightly coupled with the specifics of
the individual use case and in this case the message sequence use case.
The core of the use case is captured in the construction of model features and the design matrix to be used in the estimation process and the
automated model build component is specialized to translate the use case into an appropriate design matrix.

Because of the modular nature of the platform only the model formulation need change with changes in the target use case for the learning module.
The diagram below shows that general that is applcable for multiple use cases.

\begin{center}
\fbox{\includegraphics[width=6in]{DesignMatrix.png}}
\end{center}

The structure in the design matrix expresses the details of the use case.
The estimation of the parameters that are used to define the mapping from the design matrix to the target (the column on the right of the diagram)
capture the information in the data in a way that it can be leveraged in multiple ways including the scoring the HCPs.
The diagram shows multiple target on the right in the message sequence use case the target is either message open or message click.

Notice that in the general use case the rows of the matrix are labeled HCP x Time.
In the Message Sequence use case there is a separate design matrix for each message that is a target of prediction and the rows are HCP for HCPs that
were sent the message at least once.
Note that the HCP is replicated each time they open the message or are sent the message and did not open. 

\subsection*{Model Formulation}

To be precise and not have ambiguity in the translation of the use case and business objective to the implementation it is desirable to
formalize the details of the feature extraction and design matrix construction.
This section tries to capture enough detail to avoid ambiguity without making the discription too complex.
Even so, it is somewhat dense but hopefully will enable others to understand and decipher the implementation.

Let an event be either a message send, a message open, or a message click, so
\begin{center}
\begin{tabular}{ll}
$E_{a,i}(s,t)$ & be the $i$th event for account $a$ from time $s$ to time $t$\\
  $N_a(s,t)$ & be the number of events for account $a$ from time $s$ to time $t$\\
  $M$ & be the set of all messages \\
\end{tabular}
\end{center}
The events are ordered by time so that if the time when $E_{a,i}(0,t)$ occurs is earlier then the time when event $E_{a,j}(0,t)$ occurs then $i<j$.
\begin{equation*}
  \mathbf {E}_a(s,t) = \lbrace E_{a,i}(s,t)\;|\;0\leq i \leq N_a(s,t)\rbrace
\end{equation*}
is an ordered set of all events for the account from time $s$ to time $t$. 

Suppose we want to predict the likelihood for a particular event type to occur based on the past history of events.
Conside a target message $m^*$ that such that we want to predict the probability for an open event for message $m^*$ for an arbitrary account conditioned on the set of previous events.

For example, suppose the sequence of previous events is a sequence like send $m$, send $n$, open $m$, send $n$, send $m^*$, open $n$, and so on.
Then an observation of $\mathbf{E}_a(0,t)$ is denoted
\begin{equation*}
  e_a(0,t) = \lbrace\mbox{send }m,\mbox{send }n,\mbox{open }m,\mbox{send }n,\mbox{send }m^*,\mbox{open }n\rbrace.
\end{equation*}
For an account with this sequence we want to know
\begin{equation*}
  \mbox{pr}(\mbox{open }m^*\epsilon\;\mathbf{E}_a(0,T)\;|\;e_a(0,t) \mbox{ for } T > t)
\end{equation*}
We want to estimate this probability for each message in $M$ and for today $t$.
To estimate this probability, we build a design matrix for each message $m$ in $M$.
The rows of the matrix include data for each account that has had the target message $m^*$ either sent
to them or have opened (or clicked) on the target message.
This matrix captures the events that preceeded the target event or all the events if there was a send of the target message and no target event.
It also includes any additional demographic and/or segmentation variables that characterize the account as shown in
the design matrix diagram shown above.
The target vector is a sequence of zeros and ones depending on whether the target message $m^*$ is opened (or clicked).

\subsection*{Design Matrix}

Here we formalize the construction of the design matrix.
Let $t^{o(m,1)}_a$ be the time account $a$ first opens target message $m^*$, denoted $o(m^*,1)$. Then,
\begin{equation*}
  e_a(0,t^{o(m^*,1)}_a) \mbox{ is the set of events that occurred until message $m^*$ is first opened and}
\end{equation*}
\begin{equation*}
e_a(t^{o(m^*,1)}_a,t^{o(m^*,2)}_a) \mbox{ is the set of events that occured from the time that message $m^*$ }
\end{equation*}
is first opened and when it is next opened. If $t^{o(m^*,k)}_a$ is not defined then it is the time of the last event.

Each row in the following design matrix contains the data $e_a(t^{o(m^*,i)}_a,t^{o(m^*,i+1)}_a) $ for account $a$ where
$t^{o(m^*,0)}_a=0$ and if $t^{o(m^*,i+1)}_a$ is not defined it's the end of the horizon.
The data are represented in the matrix by either $0$s and $1$s depending on whether to event occurs for that account.
So the events that preceed a target event.
The columns of the matrix are the all the possible sends, opens, and clicks for each message in addition to the any covariates
that capture account characteristics.
The example below shows a portion of the matrix with send, open, and click columns.
If there were covariates those columns would also be included.

\begin{equation*}
\begin{array}{lcccccccccccc}
         & s(m_1,1) & s(m_2,1) & ... & s(m_2,1) & s(m_2,2) & ... & o(m_1,1) & o(m_2,1) & ... & c(m_1,1) & c(m_2,1) & ... \\
  a_1  \\
  a_1  \\
  a_2  \\
  a_3  \\
\end{array}
\end{equation*}

Note that the covariates are completely enumerated if they are discrete and if continuous they are specified by quantile so that the matrix is a boolean matrix.
Also notice that an account can appear in multiple rows if they have multiple occurrances of the target event, for example
multiple opens of $m^*$.
Finally, as mentioned above the target vector is a sequence of $0$'s and $1$'s where the element is a $1$ if the account
has opened (clicked) on the message and a $0$ if the account had not opened (clicked) on the message even though it was sent.

\subsection*{Parameter Estimation}

There are numerous ways to map the relationship between the design matrix and the target vector.
We support two types of mappings that are supported by the implementation, one is non-parametric and the other is parameteric:
(1) random forests, a nonlinear non-parametric model, and (2) elastic-net, a logistic regression based model.
The random forest is the default since it is very robust, supports very general relationships in the data, and is fast.
Changes in the use case from automated decisioning to insight generation might motivate switching to the elastic net implementation.

If the elastic-net method is used to capture the mapping between the design and target the parameters of the logistic regression
can be analyzed to understand the relationship of the various events and covariates in driving the target event.
The implementation captures that information.
Since the default method is random forests we do not show the of the logistic regression.
Below is sample output from the random forests that shows the variable importance of the various drivers.

The first six rows in the spreadsheet are the covariates in this model and the following rows are the predecessor events to the target event.
The columns show each of the target events.
In the example below the first column is the target, click on message 1004.
The values in the column below it are the normalized variable importance measures for each of the predictors (the row names)
on that target.

\begin{center}
\fbox{\includegraphics[width=6in]{VariableImportance.png}}
\end{center}

Notice that many models are included in this run as each column is for a different target event.
A separate design matrix was constructed for each of these targets and a separate random forest model
was ``solved'' for each of the targets. 
Whenever a model is built this spreadsheet is constructed and
captured on the server in a directory structure tied to the individual model runs as discussed in
more detail below in the Implementation section.

\subsection*{Model Performance}

There are several measures of model performance that are calculated and reported.
These are the AUC measure and several others based on the confusion matrix from the model fit.
The confusion matrix rows are the actual values observed in the data and the columns are the
decisions that are based on using the model as a classifier or predictor.
In the sample in the following figure the actual no (yes) are whether the account opened (clicked)
on the target message. The predicted no (yes) are whether the model predicted that the account would open (click)
on the target message.

\begin{center}
\includegraphics[width=3in]{ConfusionMatrix.png}
\end{center}

The entries in the cells are the counts for the number of events that fall into that category.
For example, the cell labeled TP=100 says that there were 100 events that were actual opens and that
the model would predict to open.
The label TP (True Positive) is used as a reference for the count in that cell.
Also, note the row and column sums shown in the figure.

The following table defines the standard statistics that are used to evaluate the performance
of a model that will be used as a predictor.
\begin{description}
  \item[AUC] Area under the (ROC) curve measures how well the model differentiates the target. Values between 0 and 1 are possible where 1 or 0 indicates perfect differentiation and .5 indicates no differentiation.
  \item[Accuracy] (TP+TN)/TOTAL measures overall how often the classifier derived from the model is correct.
  \item[Misclassification] (FP+FN)/TOTAL measures overall how often the classifier derived from the model is wrong.
  \item[TPR] TP/(FN+TP) is the true positive rate. This measures how often the classifier predicts 1 when the actual is 1.
  \item[FPR] FP/(TN+FP) is the false positive rate. This measures how often the classifier predicts 1 when the actual is 0.
  \item[Specificity] TN/(TN+FP) measures how often the classifier predicts 0 when the actual is 0.
  \item[Precision] TP/(FP+TP) measures how often the classifier predicts 1 when the actual is 0.
  \item[Prevalence] (FN+TP)/TOTAL measures how often the actual is 1 in the sample.
\end{description}

These statistics are reported in the DSE database in the LE\_MessageSequence\_Stats table for each model.
Notice that the AUC is also reported but that it is a statistic that is not derived from the confusion matrix.

\section*{Usage}

This section overviews some of the implementation detail from the perspective of usage.
It is not meant to be comprehensive but is intended to provide insight into the input and output of the algorithm.
First, it is important to understand that implementation is divided into two steps: (1) model build and (2) model scoring.
The {\em model build} is the process where for each identified target and the identified predictors a model is built
that captures the mapping from the predictors to the target.
The {\em model scoring} is the process of taking the saved model and scoring each message for each account.
The scoring step is the prediction and classifier step.
Typically, the data do not change rapidly across the engire population so that the behavior that a model
captures will remain accurate for a period of time (monthly but at the system discretion).
However, individual account and rep actions change daily which might trigger significant changes in the likelihood
of an account likelihood to open (click) on a message.
The scoring can be done nightly.

\subsection*{Model Build}

You have some limited ability to specify parameters that control the model building code.
These parameters are specified in the DSE database in the LE\_MessageSequence\_Parms table,
an example is shown below.

\begin{center}
\fbox{\includegraphics[width=6in]{Parameters.png}}
\end{center}

The first parameter named addPredictorsFromAccountProduct lists the covariates whose
data is pulled from either the Account or the AccountProduct tables in the DSE database.
The next parameter is the productName.
It is important to note that the spelling and capitalization of these field names from the AccountProduct table and the
product name must exactly match that found in the database.

The next two parameters are used to control the random forest algorithm and are typically used to avoid overfitting.
The next parameter is a switch that controls whether the random forest or the elastic net algorithm should be used.

The messageNames parameter identifies the set of messages that will be targets of the model build.
If no messageNames are given then all messages in the database will be used in the model build.

The next three parameters identify whether the message sends, opens, and clicks should be used as predictors.
By default all of those events are used, however, if any of these parameters is set to $1$ then that set of events
is not used as predictors.
This means that the design matrix will not have columns associated with those events.

The final parameter identifies the type of target. There are two choices ``OPEN'' or ``CLICK''.

\subsubsection*{Model Runs}
The run history is captured in a DSE database in the LE\_MessageSequece\_Runs table.
This table has a character field with the name of the run as the first word in the field.
An example is shown below.

\begin{center}
\fbox{\includegraphics[width=6in]{Runs.png}}
\end{center}

The field also lists the name of the program that was run, the configuration used, values of
parameters that the program used and other information.
Notice that in the example the first four runs are model scoring and the fifth run is a model build.
Also notice that the model scoring runs used a model build 2017\_02\_06\_20\_08\_02 that was executed
earlier and appears lower down on the list.

\subsubsection*{Model Performance}
As discussed above there are estimates of several performance statistics provided with a model build.
These are provided in the DSE database in the LE\_MessageSequence\_Stats table as shown below.
The table contains the statistics for each model and target.
\begin{center}
\fbox{\includegraphics[width=6in]{Performance.png}}
\end{center}

While summary results of the modeling process are saved in the DSE database they are not used operationally by
the DSE.

\subsubsection*{Other Artifacts}

In addition to saving results from both model build and model scoring in the DSE database
the implementation saves results (like the variable importance table shown above) in a set of directories
on the server where the code runs.
These directories contain printed output, logs, plots, spreadsheets, and individual models.
The specifics of what is saved depends on the specific program that is run (model build, model score, ...).
The following figure shows an example of a module build run and a model score run.

\begin{center}
\includegraphics[width=3in]{Dir_2.png}
\end{center}

The highest level directory also contains a Configurations directory which houses all the configurations that can be run.
These are snapshots of the LE\_MessageSequence\_Parms table when a run is initiated.
Note that the specific configuration for each run is also saved in the run directory in a tab within the
spreadsheet for the run.
The Catalog.txt file contains the list of runs contained in the directories.
This is similar information to that in the LE\_MessageSequence\_Runs table discussed above.

Finally, we show the contents of a model subdirectory in the model directory from the example above.
This directory contains the actual models for each of the target messages.
References that map the specific target to the specific model are contained in one of the
tabs in the spreadsheet in the same higher level directory.

When you run the model scoring code you must identify which model run to score.
The model scoring program pulls the references to the individual target models from the spreadsheet
reads the appropriate data from the DSE database and calculates the scores for each account and each
target message which is then saved in the AccountMessageSequence table.

\begin{center}
\fbox{\includegraphics[width=3in]{Dir_3.png}}
\end{center}

\subsection*{Model Scoring}

The scoring results are also saved in the DSE database and they are used by the DSE to recommend
which message to send if the DSE, as part of it's normal logic, decides that a message needs to be sent
and the model classifier should be used to decide which message to send.

\begin{center}
\fbox{\includegraphics[width=6in]{AccountMessageSequence.png}}
\end{center}

The AccountMessageSequence table is where the model scoring output is saved.
An example is shown above.
Notice each record has an accout identifer, a message identifer,
the likelihood of open (click) estimate, the model build used that is the source of estimate,
and the date added to the table.

\section*{Summary}

Of course, there are many details not covered in this document.
But the information contained here should be sufficient to gain a good understanding
of what is being estimated, how it is being estimated, and how to obtain the estimates.

\end{document}
