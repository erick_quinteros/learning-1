library(futile.logger)
library(jsonlite)
library(RMySQL)
library(openxlsx)
library(properties)


readConfig <- function(homedir) { # mainly for local test
  homedir <<- homedir
  config <- fromJSON(sprintf("%s/common/unitTest/config.json",homedir))
  # database connection config
  dbuser <<- config$dbuser
  dbpassword <<- config$dbpassword
  dbhost <<- config$dbhost
  port <<- as.numeric(config$dbport)
  dbname <<- config$dbname
  if (is.na(port)) {port <<- as.numeric(0)}
}
# readConfig(homedir='~/Documents/learning')
# source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))

readModuleConfig <- function(homedir, moduleName, parameters) { # for read module specific parameter
  config <- fromJSON(sprintf("%s/%s/tests/config.json",homedir,moduleName))
  if (length(parameters) <= 1) {
    return (config[[parameters]])
  } else {
    config_list = list()
    for (param in parameters) {
      config_list[[param]] <- config[[param]]
    }
    return (config_list)
  }
}

processConfig <- function(dbname) { # get _cs, _learning,_stage DB name and match with other mockdata file
  dbname_cs <- paste0(dbname,"_cs")
  dbname_learning <- paste0(dbname,"_learning")
  dbname_stage <- paste0(dbname,"_stage")
  dbname_archive <- paste0(dbname,"_archive")
  dbname_filepath_match <- list(dbname, dbname_learning, dbname_stage, dbname_cs, dbname_archive)
  names(dbname_filepath_match) <- c('pfizerusdev','pfizerusdev_learning','pfizerusdev_stage','pfizerprod_cs', 'pfizerusdev_archive')
  return (dbname_filepath_match)
}
# dbname_filepath_match <<- processConfig(dbname)

readMockData <- function(tablename, dbname_ind, dbname_filepath_match) { # readmock data given its original dbname and tablename, dbname_ind based on ['pfizerprod_cs','pfizerusdev','pfizerusdev_stage','pfizerusdev_learning']
  filepath <- sprintf('%s/common/unitTest/data/%s/%s.csv', homedir, names(dbname_filepath_match)[dbname_ind], tablename)
  data <- read.csv(file=filepath, header=TRUE, sep=",", as.is=TRUE)
  return (data)
}

writeMockDataToDB <- function(con,filepath,truncateFirst=FALSE) {
  file <- basename(filepath)
  tableName <- sub('\\.csv$', '', file)
  if (truncateFirst) {
    dbClearResult(dbSendQuery(con, sprintf("TRUNCATE %s ;",tableName)))
  }
  if (file.exists(filepath)) {
    data <- read.csv(file=filepath, header=TRUE, sep=",", as.is=TRUE)
    dbWriteTable(con,tableName,data, row.names=FALSE, overwrite=FALSE, append=TRUE)
    flog.info(sprintf('finish resetting mock data to Table: %s',tableName))
  } else {
    flog.info('no mock data for Table: %s, leave table blank',tableName)
  }
}

# function to read create table SQL individually for later single use
setupTable <- function(con, filepath, dropFirst=FALSE) {
  file <- basename(filepath)
  tableName <- sub('\\.sql$', '', file)
  # drop table if exists already
  if (dropFirst) {
    dbClearResult(dbSendQuery(con, sprintf('DROP TABLE IF EXISTS %s',tableName)))
  }
  # read create table sql
  ff <- file(filepath,'r')
  SQL <- paste(readLines(ff, warn=FALSE),collapse='')
  close(ff)
  # execute query
  dbClearResult(dbSendQuery(con, SQL))
  flog.info('finish create Table: %s',tableName)
}

resetMockData <- function(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList=NULL, requiredMockDataList_module=NULL,module_dir=NULL) { # this func run before every unit test
  # setup dbname config
  dbname_filepath_match <- processConfig(dbname)
  
  if (!is.null(requiredMockDataList_module) & is.null(module_dir)) { # check to make sure module_dir must be provided if requiredMockDataList_module is not null
    stop('ERROR: if given requiredMockDataList_module, must give module_dir also')
  }
  drv <- dbDriver("MySQL")
  con_db <- dbConnect(drv,user=dbuser,host=dbhost,port=as.numeric(port),password=dbpassword)
  db_list <- names(dbname_filepath_match)
  for (db in db_list) {
    # first get test dbname
    testDBName <- dbname_filepath_match[[db]]
    flog.info('Set mock data for schema:%s...',testDBName)
    # initialize set to hold mock data filepaths and create table sql filepath
    filesToWrite <- character()
    filesToCreateTable <- character()
    # read local data and write to db
    dbSetupSQLPath <- paste(homedir,'/common/unitTest/dbSetup/',db,sep='')
    commonDataPath <- paste(homedir,'/common/unitTest/data/',db,sep='')
    if (is.null(requiredMockDataList)) { # write all mock data in the folder if no table spcified
      filesToWrite <- c(filesToWrite, list.files(path=commonDataPath, full.names=TRUE))
      filesToCreateTable <- c(filesToCreateTable, list.files(path=dbSetupSQLPath, full.names=TRUE))
    } else if (!is.null(requiredMockDataList[[db]])) {
      filesToWrite <- c(filesToWrite, paste(commonDataPath,'/',requiredMockDataList[[db]],'.csv',sep=''))
      filesToCreateTable <- c(filesToCreateTable, paste(dbSetupSQLPath,'/',requiredMockDataList[[db]],'.sql',sep=''))
    }
    if (!is.null(requiredMockDataList_module) & !is.null(requiredMockDataList_module[[db]])) {
      moduleDataPath <- paste(homedir,'/',module_dir,'/tests/data/',db,sep='')
      filesToWrite <- c(filesToWrite, paste(moduleDataPath,'/',requiredMockDataList_module[[db]],'.csv',sep=''))
      filesToCreateTable <- c(filesToCreateTable, paste(dbSetupSQLPath,'/',requiredMockDataList_module[[db]],'.sql',sep=''))
    }
    # create database first if not exists
    dbClearResult(dbSendQuery(con_db, sprintf("CREATE SCHEMA IF NOT EXISTS %s CHARACTER SET utf8 COLLATE utf8_unicode_ci;",testDBName)))
    # connect to DB
    con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=testDBName,port=port,password=dbpassword)
    # clear DB if there is already data
    dbTables <- dbGetQuery(con_db, sprintf("SELECT table_name FROM information_schema.tables WHERE table_schema='%s';",testDBName))[["table_name"]]
    lapply(dbTables,function(x) {dbClearResult(dbSendQuery(con, sprintf("DROP TABLE %s;",x)))})
    # write mock data if needs
    if (length(filesToWrite)>0 & length(filesToCreateTable)>0 & (length(filesToWrite)==length(filesToCreateTable)) | is.null(requiredMockDataList)) {
      # setup table
      for (ind in 1:length(filesToCreateTable)) {
        setupTable(con, filesToCreateTable[ind])
      }
      # fill in mock data
      for (ind in 1:length(filesToWrite)) {
        writeMockDataToDB(con,filesToWrite[ind])
      }
    }
    # disconnect DB
    dbDisconnect(con)
  }
  dbDisconnect(con_db)
}

changeModelSavePath <- function(xlsxFile, updateModelSaveFileSheetNo, updateModelSaveFileColName, homedir, buildUID) {
  print(buildUID)
  # read xlsx
  xlsxWorkbook <- loadWorkbook(xlsxFile)
  for (sheetNo in as.list(updateModelSaveFileSheetNo)) {
    models <- readWorkbook(xlsxWorkbook, sheet=sheetNo)
    # replace the hard coded learning path to current learning repo dir
    modelSavePath <- models[[updateModelSaveFileColName]]
    models[[updateModelSaveFileColName]] <- paste(homedir,"builds",buildUID,sapply(strsplit(basename(dirname(modelSavePath)), split='_'), function(x) paste(paste(x[1:length(x)-1], collapse='_'), buildUID, sep="_")), basename(modelSavePath),sep='/')
    # write back the the excel file
    writeData(xlsxWorkbook, sheet=sheetNo, models)
  }
  saveWorkbook(xlsxWorkbook, file=xlsxFile, overwrite=TRUE)
}

copyMockBuildDir <- function(homedir, moduleName, BUILD_UID, files_to_copy=NULL, new_BUILD_UID=NULL, new_configUID=NULL, new_versionUID=NULL, updateModelSaveFileName=NULL, updateModelSaveFileSheetNo=3, updateModelSaveFileColName='modelName') {
  mock_BUILD_UID <- ifelse(is.null(new_BUILD_UID), BUILD_UID, new_BUILD_UID)
  # create/clean build dir
  buildUIDDirpath <- paste(homedir,'/builds/',mock_BUILD_UID,sep='')
  if (! dir.exists(buildUIDDirpath)) { # generate buildUID folder
    dir.create(buildUIDDirpath, recursive=TRUE)
  } else { # clean buildUID folder
    unlink(paste(buildUIDDirpath,'/*',sep=''),recursive = TRUE, force = TRUE)
  }
  # figure out files to copy
  if (is.null(files_to_copy)) {
    files_to_copy <- list.files(path=sprintf('%s/%s/tests/data/%s',homedir,moduleName,BUILD_UID), full.names=TRUE)
  } else {
    files_to_copy <- paste(sprintf('%s/%s/tests/data/%s/',homedir,moduleName,BUILD_UID), files_to_copy, sep='')
  }
  file.copy(files_to_copy, buildUIDDirpath, recursive=TRUE)
  # if BUILD_UID change, make corresponding changes
  if (!is.null(new_BUILD_UID)) {
    # modify buildUID in config
    config <- read.properties(sprintf('%s/builds/%s/learning.properties',homedir,mock_BUILD_UID))
    config[['buildUID']] <- new_BUILD_UID
    if (!is.null(new_configUID)) { config[['configUID']] <- new_configUID  }
    if (!is.null(new_versionUID)) { config[['versionUID']] <- new_versionUID }
    write.properties(sprintf('%s/builds/%s/learning.properties',homedir,mock_BUILD_UID),config)
    # rename file
    files_in_folder <- list.files(path=sprintf('%s/builds/%s',homedir,new_BUILD_UID), full.names=FALSE)
    file.rename(paste(sprintf('%s/builds/%s',homedir,new_BUILD_UID),files_in_folder,sep='/'), paste(sprintf('%s/builds/%s',homedir,new_BUILD_UID),gsub(BUILD_UID, new_BUILD_UID, files_in_folder),sep='/'))
  }
  # update model saved path in excel file
  if (is.null(updateModelSaveFileName)) {updateModelSaveFileName <- sprintf('%sDriver.r_%s.xlsx', moduleName, mock_BUILD_UID, mock_BUILD_UID)}
  xlsxFile <- sprintf('%s/builds/%s/%s',homedir,mock_BUILD_UID, updateModelSaveFileName)
  if (file.exists(xlsxFile)) {
    changeModelSavePath(xlsxFile, updateModelSaveFileSheetNo, updateModelSaveFileColName, homedir, mock_BUILD_UID)
  }
  return (mock_BUILD_UID)
}

setupMockBuildDir <- function(homedir, moduleName, BUILD_UID, files_to_copy=NULL, forNightly=FALSE, new_BUILD_UID=NULL, new_configUID=NULL, new_versionUID=NULL, updateModelSaveFileName=NULL, updateModelSaveFileSheetNo=3, updateModelSaveFileColName='modelName') {
  # copy saved builds for unit test to build folder
  mock_BUILD_UID <- copyMockBuildDir(homedir, moduleName, BUILD_UID, files_to_copy, new_BUILD_UID, new_configUID, new_versionUID, updateModelSaveFileName, updateModelSaveFileSheetNo)
  # deploy builf if forNightly
  if (forNightly) {
    configUID <- read.properties(sprintf('%s/builds/%s/learning.properties',homedir,mock_BUILD_UID), fields="configUID")
    nightlyDirpath <- paste(homedir,'builds/nightly',configUID, sep='/')
    if (! dir.exists(nightlyDirpath)) { # generate buildUID folder
      dir.create(nightlyDirpath, recursive=TRUE)
    } else {
      unlink(paste(nightlyDirpath,'/*',sep=''),recursive = TRUE, force = TRUE)
    }
    file.copy(sprintf('%s/builds/%s/learning.properties',homedir,mock_BUILD_UID), nightlyDirpath)
  }
}
