import unittest
import sys
import os
import pandas as pd
import io

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
unitTest_dir = os.path.dirname(script_dir)
common_dir = os.path.dirname(unitTest_dir)
sys.path.append(common_dir)

from common.pyUtils.database_utils import DatabasePoolConnection, other_database_op
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig


class DBresetMockData():
    def __init__(self, test_dir, host, user, password, port, dbname):
        self.test_dir = test_dir
        self.dbhost = host
        self.dbuser = user
        self.dbpassword = password
        self.dbport = port
        self.dbname_dse = dbname
        self.dbname_learning = self.dbname_dse + '_learning'
        self.dbname_stage = self.dbname_dse + '_stage'
        self.dbname_cs = 'pfizerprod_cs' if self.dbname_dse[:8] == 'pfizerus' else self.dbname_dse + '_cs'
        self.db_conn_pool = None
        self.db_map = {"dse": self.dbname_dse, "learning":self.dbname_learning, "stage":self.dbname_stage, "cs":self.dbname_cs}
        self.filename_map = {"dse":"pfizerusdev", "learning":"pfizerusdev_learning", "stage":"pfizerusdev_stage", "cs":"pfizerprod_cs"}

    def connnect_to_db(self):
        self.db_conn_pool = DatabasePoolConnection(max_overflow=5, pool_size=2, dbuser=self.dbuser, dbpassword=self.dbpassword, dbhost=self.dbhost, port=self.dbport)

    def dropoldtables(self):
        for schema in self.db_map:
            # print(self.db_map[schema])
            query = "DROP DATABASE IF EXISTS {}".format(self.db_map[schema])
            other_database_op(query, self.db_conn_pool)

    def readcsv(self, schema, table):
        csvpath = self.test_dir + "/data/" + self.filename_map[schema] + "/" + table + ".csv"
        if not os.path.exists(csvpath):
            #if there is no local to the module data to override, use the data in common/unitTest
            csvpath = "common/unitTest/data/" + self.filename_map[schema] + "/" + table + ".csv"
        df = pd.read_csv(csvpath)
        # print(df)
        return df

    def writedata(self, schema, table, csvdf):
        csvdf.to_sql(name=table, con=self.db_conn_pool.get_conn(), schema=self.db_map[schema], if_exists="append", index=False)

    def buildtable(self, schema, table):
        sqlpath = "common/unitTest/dbSetup/" + self.filename_map[schema] + "/" + table + ".sql"
        file = open(sqlpath, "r")
        query = file.read()
        print(query)
        file.close()
        other_database_op(query, self.db_conn_pool)
        csvdf = self.readcsv(schema, table)
        self.writedata(schema, table, csvdf)

    def close_pool(self):
        self.db_conn_pool.close_all_conn()

    def run(self, requiredDataList={"dse":[], "cs":[], "stage":[], "learning":[], "archive":[]}):
        self.connnect_to_db()
        self.dropoldtables()
        for schema in self.db_map:
            query = "CREATE DATABASE IF NOT EXISTS {} CHARACTER SET utf8 COLLATE utf8_unicode_ci;".format(self.db_map[schema])
            other_database_op(query, self.db_conn_pool)
            query = "USE {}".format(self.db_map[schema])
            for table in requiredDataList[schema]:
                other_database_op(query, self.db_conn_pool)
                self.buildtable(schema, table)
        self.close_pool()
        self.db_conn_pool.get_stat()


class unitTestParent(unittest.TestCase):

    def reqDBs(self):
        required_dse = [  ]
        required_cs = [  ]
        required_stage = [  ]
        required_learning = [  ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required

    def setUp(self):
        # initialize DB connection config classs
        self.test_dir = sys.argv[1][9:-1] + "/" + sys.argv[2][9:-1]
        self.db_user = sys.argv[4][8:-1]
        self.db_password = sys.argv[5][12:-1]
        self.db_host = sys.argv[6][8:-1]
        self.dse_db_name = sys.argv[7][8:-1]
        self.db_port = sys.argv[8][5:]  # Update to 33066 for testing on Local machine
        # Get the singleton instance of the database config and set the properties

        self.learning_db_name =  self.dse_db_name + '_learning'
        self.cs_db_name = 'pfizerprod_cs' if self.dse_db_name[:8] == 'pfizerus' else self.dse_db_name + '_cs'
        self.stage_db_name = self.dse_db_name + '_stage'

        #comment out lines 108-109 and 111 to prevent SQL command logging supression
        text_trap = io.StringIO()
        sys.stdout = text_trap
        DBresetMockData(self.test_dir, self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name).run(self.reqDBs())
        sys.stdout = sys.__stdout__

    def tearDown(self):
        pass
