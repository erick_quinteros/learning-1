CREATE TABLE `RepAccountAssignment_arc` (
  `repId` int(11) NOT NULL DEFAULT '0',
  `accountId` int(11) NOT NULL DEFAULT '0',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `arcstartDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `arcendDate` datetime DEFAULT NULL,
  `scdRowTypeId` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `dateOfProcessRun` datetime NOT NULL,
  PRIMARY KEY (`repId`,`accountId`,`arcstartDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
