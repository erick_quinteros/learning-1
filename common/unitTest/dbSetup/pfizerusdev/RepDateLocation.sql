CREATE TABLE `RepDateLocation` (
  `repId` int(11) NOT NULL,
  `date` date NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `maxNearDistance` double DEFAULT NULL,
  `maxFarDistance` double DEFAULT NULL,
  `source` VARCHAR(31) DEFAULT NULL,
  `learningRunUID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;