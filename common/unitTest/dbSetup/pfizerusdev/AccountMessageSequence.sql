CREATE TABLE `AccountMessageSequence` (
  `accountId` int(11) DEFAULT NULL,
  `messageAlgorithmId` int(11) DEFAULT NULL,
  `messageId` int(11) DEFAULT NULL,
  `modelId` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probability` double DEFAULT NULL,
  `predict` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci