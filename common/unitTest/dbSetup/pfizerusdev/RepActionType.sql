CREATE TABLE `RepActionType` (
  `repActionTypeId` tinyint(4) NOT NULL,
  `repActionTypeName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repActionTypeId`),
  UNIQUE KEY `repActionType_uniqueName` (`repActionTypeName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci