CREATE TABLE `InteractionAccount` (
  `interactionId` int(11) NOT NULL,
  `accountId` int(11) NOT NULL,
  `suggestionReferenceId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matchedSuggestionRepAccount` tinyint(1) DEFAULT NULL,
  `matchedSuggestionRepAccountAction` tinyint(1) DEFAULT NULL,
  `suggestionInferredAt` datetime DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`interactionId`,`accountId`),
  UNIQUE KEY `interactionAccount_uniqueAK` (`interactionId`,`accountId`),
  KEY `interactionAccount_accountId` (`accountId`),
  KEY `interactionAccount_interactionId` (`interactionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci