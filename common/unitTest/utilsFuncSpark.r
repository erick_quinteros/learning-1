library(data.table)
library(sparklyr)

convertSDFToDF <- function(sdf, colClasses=NULL) {
  # csvSaveDir <- "/tmp/sdf_temp"
  # if (dir.exists(csvSaveDir)) {unlink(csvSaveDir,recursive = TRUE, force = TRUE)}
  # spark_write_csv(sdf, path=csvSaveDir)
  # tmp <- lapply(list.files(csvSaveDir, pattern=".csv", full.names=TRUE), function(x){fread(file=x, colClasses=colClasses)})
  # # tmp <- lapply(list.files(csvSaveDir, pattern=".csv", full.names=TRUE), function(x){read.csv(x, stringsAsFactors=FALSE, header=TRUE,na.strings="NA")})
  # df <- do.call("rbind", tmp)
  # return(df)
  return(data.table(collect(sdf)))
}
