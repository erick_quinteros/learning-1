from .logger import get_module_logger

logger = get_module_logger(__name__)

class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Also, the decorated class cannot be
    inherited from. Other than that, there are no restrictions that apply
    to the decorated class.

    To get the singleton instance, use the `instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


@Singleton
class DatabaseConfig:
    """
    This is a singleton class holds the database configuration for current instance.
    """

    def set_config(self, dbhost, dbuser, dbpassword, dbname, port=3306, **kwargs):
        """

        :param host:
        :param user:
        :param password:
        :param dbname:
        :param port:
        :return:
        """
        self.dbhost = dbhost
        self.dbuser = dbuser
        self.dbpassword = dbpassword
        self.dbport = port
        self.dbname_dse = dbname
        self.dbname_learning = self.dbname_dse + '_learning'
        self.dbname_stage = self.dbname_dse + '_stage'
        self.dbname_cs = 'pfizerprod_cs' if self.dbname_dse[:8] == 'pfizerus' else self.dbname_dse + '_cs'
        self.schema_mapping = {"dse": self.dbname_dse, "learning": self.dbname_learning, "stage": self.dbname_stage, "cs": self.dbname_cs}
        logger.info("Get DB config: {}".format(", ".join("{}={}".format(k,v) for k,v in self.__dict__.items())))