context('test the dbConnection script in the messageSequence module')
print(Sys.time())

# get connection to handle drop and create schema
library(RMySQL)
drv <- dbDriver("MySQL")
con_db <- dbConnect(drv,user=dbuser,host=dbhost,port=port,password=dbpassword)

# source funcs in script
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))

# setup test DB schema
setupTestDBConn <- function(con_db, dbname) {
  # drop DB and create DB
  dbClearResult(dbSendQuery(con_db, sprintf("DROP SCHEMA IF EXISTS %s;",dbname)))
  dbClearResult(dbSendQuery(con_db, sprintf("CREATE SCHEMA %s CHARACTER SET utf8 COLLATE utf8_unicode_ci;",dbname)))
}

# test functions
testDBConn <- function(con) {
  # test create table
  expect_true(dbClearResult(dbSendQuery(con, "CREATE TABLE testDB (id int, testValue double, testChar varchar(80));")), label="create table")
  # test insert
  expect_true(dbClearResult(dbSendQuery(con, "INSERT INTO testDB VALUES (0,2.2,'test0');")), label="insert into table")
  # test write and read data from DB
  testDF <- data.frame(id=1, testValue=2.3, testChar="test1", stringsAsFactors=FALSE)
  expect_true(dbWriteTable(con, name="testDB", value=testDF, overwrite=FALSE, append=TRUE, row.names=FALSE), label="append table")
  expect_equal(dim(dbGetQuery(con, "SELECT * FROM testDB;")), c(2,3))
  expect_true(dbWriteTable(con, name="testDB", value=testDF, overwrite=TRUE, append=FALSE, row.names=FALSE), label="overwrite table")
  expect_equal(dbGetQuery(con, "SELECT * FROM testDB;"), testDF)
  # test drop table
  expect_true(dbClearResult(dbSendQuery(con, "DROP TABLE testDB;")), label="drop table")
  # test disconnect DB
  expect_true(dbDisconnect(con), label="disconnect DB")
}

# tests for function
test_that("test getDBConnection function", {
  setupTestDBConn(con_db, dbname)
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  testDBConn(con)
})

test_that("test getDBConnectionLearning function", {
  setupTestDBConn(con_db, dbname_learning)
  con <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
  testDBConn(con)
})

test_that("test getDBConnectionStage function", {
  setupTestDBConn(con_db, dbname_stage)
  con <- getDBConnectionStage(dbuser, dbpassword, dbhost, dbname, port)
  testDBConn(con)
})

test_that("test getDBConnectionCS function", {
  setupTestDBConn(con_db, dbname_cs)
  con <- getDBConnectionCS(dbuser, dbpassword, dbhost, dbname, port)
  testDBConn(con)
})

# disconnect DB
dbDisconnect(con_db)